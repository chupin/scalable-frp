{

module Lexer where

import Syntax (Ann(..))

}

%wrapper "monad"

$digits = 0-9
$lower = a-z
$upper = A-Z
$alpha = [$lower$upper]

@ident = ("_"|$lower)($alpha|_|'|$digits)*
@hsExpr = "<-".*"-<"
@liftHsExpr = "{|".*"|}"

tokens :-
       "--".*  { skip }
       $white+ { skip }
       "proc" { mkConstTok TokProc }
       "do"   { mkConstTok TokDo }
       "out"  { mkConstTok TokOut }
       "::"   { mkConstTok TokDoubleCol }
       ";"    { mkConstTok TokSemiCol }
       ","    { mkConstTok TokComma }
       "{"    { mkConstTok TokLBrace }
       "}"    { mkConstTok TokRBrace }
       "C"    { mkConstTok (TokAnn C) }
       "S"    { mkConstTok (TokAnn S) }
       "E"    { mkConstTok (TokAnn E) }
       @ident { mkTok TokIdent }
       @hsExpr { mkTok (\s -> TokHsExpr (drop 2 (take (length s - 2) s))) }
       @liftHsExpr { mkTok (\s -> TokLiftHsExpr (drop 2 (take (length s - 2) s))) }

{

data Tok = TokProc
         | TokDo
         | TokOut
         | TokDoubleCol
         | TokSemiCol
         | TokComma
         | TokLBrace
         | TokRBrace
         | TokAnn Ann
         | TokIdent String
         | TokHsExpr String
         | TokLiftHsExpr String
         | TokEOF
         deriving(Show)

alexEOF = pure TokEOF

mkTok :: (String -> token) -> AlexAction token
mkTok f = token (\(_,_,_,s) len -> f (take (fromIntegral len) s))

mkConstTok tok = mkTok (\_ -> tok)

scanner s = runAlex s loop
  where loop = do
         tok <- alexMonadScan
         case tok of
           TokEOF -> pure []
           _ -> do toks <- loop
                   pure (tok : toks)


}
