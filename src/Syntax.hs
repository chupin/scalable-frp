{-# LANGUAGE DeriveFoldable, DeriveFunctor, DeriveTraversable, FlexibleContexts,
             OverloadedLists, RecordWildCards, TemplateHaskell #-}

module Syntax where

import           Control.Applicative        (Alternative (..))
import           Control.Arrow              (first, second)
import           Control.Monad.Except
import           Control.Monad.State
import           Control.Monad.Writer       hiding (First)
import           Data.Foldable              (toList)
import           Data.Map                   (Map)
import qualified Data.Map                   as M
import           Data.Maybe
import           Data.Sequence              (Seq (..))
import           Data.Set                   (Set)
import qualified Data.Set                   as S
import qualified Data.Tree                  as T
import qualified Language.Haskell.Meta      as H
import qualified Language.Haskell.TH.Quote  as H
import qualified Language.Haskell.TH.Syntax as H

data UndesVar = SigVar (Var String)
              | SigHsExp H.Exp

data Ann = C | S | E
         deriving(Show)

data Var id = Null
            | Var id (Maybe Ann)
            deriving(Functor, Foldable, Traversable)

instance Show id => Show (Var id) where
  show Null                = "Null"
  show (Var id Nothing)    = show id
  show (Var id (Just ann)) = show id ++ "::" ++ show ann

instance Eq id => Eq (Var id) where
  Null == Null = True
  Var i1 _ == Var i2 _ = i1 == i2
  _ == _ = False

instance Ord id => Ord (Var id) where
  Null `compare` Null = EQ
  Null `compare` Var _ _ = LT
  Var _ _ `compare` Null = GT
  Var i1 _ `compare` Var i2 _ = i1 `compare` i2

data Sig sig = Sig sig
             | Vec (Sig sig) (Sig sig)
             deriving(Eq, Ord, Functor, Foldable, Traversable)

instance Show id => Show (Sig id) where
  show = T.drawTree . toDataTree
    where toDataTree (Sig sig) = T.Node (show sig) []
          toDataTree (Vec l r) = T.Node "" [toDataTree l, toDataTree r]

countLeaves :: Sig a -> Int
countLeaves = length

countNulls :: Ord id => Sig (Var id) -> Int
countNulls = length . filter (== Null) . toList

sigVars :: Ord id => Sig (Var id) -> Map id Int
sigVars (Sig Null)      = []
sigVars (Sig (Var x _)) = [(x, 1)]
sigVars (Vec s1 s2)     = M.unionWith (+) (sigVars s1) (sigVars s2)

data Statement input output =
  Statement { stmtInput  :: Sig input
            , stmtOutput :: Sig output
            , stmtBody   :: H.Exp
            }
  deriving(Show)

-- Input and output
data Fun input output =
  Fun { funInput  :: Sig output
      , funOutput :: Sig input
      , funBody   :: Seq (Statement input output)
      }
  deriving(Show)

data Err = NotInScope String
         | BoundTwice String

instance Show Err where
  show (NotInScope id) = "Not in scope " ++ show id
  show (BoundTwice id) = "This identifier " ++ show id ++
                         " is bound several time in the same signal pattern"

data Ident = Ident { identSrc :: Maybe String
                   , identIdx :: Int
                   }
           deriving(Eq, Ord)

instance Show Ident where
  show (Ident src idx) = fromMaybe "" src ++ "_" ++ show idx
