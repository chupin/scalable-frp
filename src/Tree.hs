{-# LANGUAGE FlexibleContexts, OverloadedLists, ViewPatterns #-}

module Tree where

import           Control.Arrow
import           Control.Monad.State
import qualified Data.Map            as M
import           Data.Maybe
import qualified Data.Tree           as T
--import Data.Sequence (Seq (..))

data Tree a = Leaf a
            | Node (Tree a) (Tree a)

toDataTree :: Show a => Tree a -> T.Tree String
toDataTree (Leaf x)   = T.Node (show x) []
toDataTree (Node l r) = T.Node "" [toDataTree l, toDataTree r]

instance Show a => Show (Tree a) where
  show = T.drawTree . toDataTree

leafCount :: Tree a -> Int
leafCount (Leaf _)          = 1
leafCount (Node left right) = leafCount left + leafCount right

data Router = Id
            | Fst
            | Snd
            | RightRot
            | LeftRot
            | Swap
            | Dup
            | AndThen Router Router
            | First Router
            | Second Router
            deriving(Show)

simplifyRouter :: Router -> Router
simplifyRouter (r1 `AndThen` r2) =
  case (simplifyRouter r1, simplifyRouter r2) of
    (Id, r2)            -> r2
    (r1, Id)            -> r1
    (LeftRot, RightRot) -> Id
    (RightRot, LeftRot) -> Id
    (r1, r2)            -> r1 `AndThen` r2
simplifyRouter (First r) =
  case simplifyRouter r of
    Id -> Id
    r  -> First r
simplifyRouter (Second r) =
  case simplifyRouter r of
    Id -> Id
    r  -> Second r
simplifyRouter rout = rout

applyRouter :: Show a => Router -> Tree a -> Tree a
applyRouter Id tree = tree
applyRouter Fst (Node left _) = left
applyRouter Snd (Node _ right) = right
applyRouter RightRot (Node (Node leftLeft leftRight) right) =
  Node leftLeft (Node leftRight right)
applyRouter LeftRot (Node left (Node rightLeft rightRight)) =
  Node (Node left rightLeft) rightRight
applyRouter Swap (Node left right) = Node right left
applyRouter Dup tree = Node tree tree
applyRouter (r1 `AndThen` r2) tree = applyRouter r2 (applyRouter r1 tree)
applyRouter (First r) (Node left right) = Node (applyRouter r left) right
applyRouter (Second r) (Node left right) = Node left (applyRouter r right)
applyRouter r t =
  error ("Cannot apply router " ++ show r ++ " to tree:\n" ++ show t)

-- Matches the number of leaves in the original tree with the number
-- of leaves in the destination tree
matchLeafCount :: (Show a, Ord a) => Tree a -> Tree a -> (Tree a, Router)
matchLeafCount orig dest = evalState (fromJust <$> go orig) diffMap
  where countLeaves (Leaf x) = [(x, 1)]
        countLeaves (Node left right) =
          M.unionWith (+) (countLeaves left) (countLeaves right)

        leavesInOrigin = countLeaves orig
        leavesInDest = countLeaves dest

        diffMap =
          M.unionWith (+) leavesInDest (fmap negate leavesInOrigin)

        go tree@(Leaf v) = do
          diffMap <- get
          case M.lookup v diffMap of
            Just diff
              | diff == 0 -> pure (Just (tree, Id))
              -- There are more leaves of this type in the target. We
              -- should duplicate
              | diff > 0 -> do
                  modify (M.adjust pred v)
                  res <- go (Node tree tree)
                  pure (fmap (second (Dup `AndThen`)) res)
              -- There are too many leaves of this type in the
              -- origin. We drop this one.
              | diff < 0 -> do
                  modify (M.adjust succ v)
                  pure Nothing
            Nothing -> error ("The leaf " ++ show v ++ " is unknown")

        go (Node left right) = do
          newLeft <- go left
          newRight <- go right
          case (newLeft, newRight) of
            (Nothing, Nothing) ->
              pure Nothing
            (Just (left, leftTrans), Nothing) ->
              pure (Just (left, Snd `AndThen` leftTrans))
            (Nothing, Just (right, rightTrans)) ->
              pure (Just (right, Fst `AndThen` rightTrans))
            (Just (left, leftTrans), Just (right, rightTrans)) ->
              pure (Just (Node left right, First leftTrans `AndThen` Second rightTrans))

listify :: Tree a -> ([a], Router)
listify (Leaf a) = ([a], Id)
listify (Node (Leaf a) right) = (a : listRight, Second trans)
  where (listRight, trans) = listify right
listify (Node (Node leftLeft leftRight) right) = (list, RightRot `AndThen` trans)
  where rotatedTree = Node leftLeft (Node leftRight right)
        (list, trans) = listify rotatedTree

sortListifyPass :: Ord a => Int -> [a] -> ([a], Router)
sortListifyPass _ [] = ([], Id)
sortListifyPass 0 sorted = (sorted, Id)
sortListifyPass n (v1 : (sortListifyPass (n - 1)-> (vs, trans))) =
  case vs of
    [] -> ([v1], Id)
    (v2 : vs) | v1 <= v2 -> (v1 : v2 : vs, Second trans)
              | otherwise ->
                case vs of
                  [] -> ([v2, v1], Swap)
                  _  -> (v2 : v1 : vs, Second trans `AndThen` swapHeads)
    where swapHeads = LeftRot `AndThen` First Swap `AndThen` RightRot

sortListify :: Ord a => [a] -> ([a], Router)
sortListify list = go Id (length list) list
  where go prev 0 list = (list, prev)
        go prev n list = go (prev `AndThen` trans) (n - 1) sorted
          where (sorted, trans) = sortListifyPass n list

delistify :: [a] -> Tree a
delistify []       = error "Impossible"
delistify [x]      = Leaf x
delistify (x : xs) = Node (Leaf x) (delistify xs)

sortTree :: Ord a => Tree a -> (Tree a, Router)
sortTree tree = (sortedTree, toListTrans `AndThen` toSortedTrans)
  where (list, toListTrans) = listify tree
        (sortedList, toSortedTrans) = sortListify list
        sortedTree = delistify sortedList

matchSortedTree :: Show a => Tree a -> Tree a -> Router
matchSortedTree (Leaf _) (Leaf _) = Id
matchSortedTree start@(Node l1 r1) dest@(Node l2 r2) =
  case leafCount l1 `compare` leafCount l2 of
    EQ -> First (matchSortedTree l1 l2) `AndThen` Second (matchSortedTree r1 r2)
    -- There are more nodes on the left that there should. We should
    -- thus do a right rotation and try to match again
    GT -> RightRot `AndThen` trans
      where trans = matchSortedTree (applyRouter RightRot start) dest
    LT -> LeftRot `AndThen` trans
      where trans = matchSortedTree (applyRouter LeftRot start) dest

matchTree :: (Ord a, Show a) => Tree a -> Tree a -> Router
matchTree start dest =
  simplifyRouter (toSameCount `AndThen` toSorted `AndThen` toMatch)
  where (sameLeafCountTree, toSameCount) = matchLeafCount start dest
        (sortedStart, toSorted) = sortTree sameLeafCountTree
        toMatch = matchSortedTree sortedStart dest

orig :: Tree Int
orig = Node (Node (Node (Leaf 1) (Leaf 1)) (Leaf 5))
            (Node (Node (Leaf 2) (Node (Leaf 4) (Leaf 3)))
                  (Node (Leaf 6) (Leaf 7)))

dest :: Tree Int
dest = Node (Node (Leaf 1) (Node (Leaf 2) (Leaf 3)))
            (Node (Node (Leaf 4) (Node (Leaf 5) (Leaf 6))) (Leaf 7))
