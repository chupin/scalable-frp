module TypLexer where

import Data.Char
import Syntax    (Ann (..))

data Tok = TokHsTyp String
         | TokAnn Ann
         | TokLBrace
         | TokRBrace
         | TokDoubleCol
         | TokComma
         | TokEOF
         deriving(Show)

start :: String -> [Tok]
start ('{' : xs) = TokLBrace : start xs
start ('}' : xs) = TokRBrace : start xs
start (',' : xs) = TokComma : start xs
start (c : xs)   | isSpace c = start xs
start xs@(_ : _) = typ xs
start _          = []

typ :: String -> [Tok]
typ = go []
  where go typ (':' : ':' : xs) = TokHsTyp (reverse typ) : TokDoubleCol : ann xs
        go typ (c : xs)         = go (c : typ) xs
        go typ []               = [TokHsTyp (reverse typ)]

ann :: String -> [Tok]
ann cs =
  case cs of
    (c : cs)   | isSpace c -> ann cs
    ('C' : cs) -> TokAnn C : start cs
    ('S' : cs) -> TokAnn S : start cs
    ('E' : cs) -> TokAnn E : start cs
    _          -> error "Lexical error: expected C, S, E or a space"

scanTyp :: String -> Either String [Tok]
scanTyp = Right . start
