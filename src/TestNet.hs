{-# LANGUAGE TemplateHaskell #-}

module TestNet where

import           ClassSF
import qualified Language.Haskell.TH.Quote  as QQ
import qualified Language.Haskell.TH.Syntax as TH
import           Quote
import           Scale
import           System.Random

net :: Network
net = randomNetwork (mkStdGen 100) 100

strNetSFRP :: String
strNetSFRP =
  $(pure (TH.LitE (TH.StringL (printSFRP (randomNetwork (mkStdGen 100) 100)))))

strNetYampa :: String
strNetYampa =
  $(pure (TH.LitE (TH.StringL (printYampa (randomNetwork (mkStdGen 100) 100)))))
