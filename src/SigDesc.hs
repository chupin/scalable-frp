{-# LANGUAGE DataKinds, EmptyCase, FlexibleContexts, FlexibleInstances,
             FunctionalDependencies, GADTs, KindSignatures,
             MultiParamTypeClasses, PartialTypeSignatures, PolyKinds,
             RankNTypes, RecursiveDo, ScopedTypeVariables, TypeFamilies,
             TypeInType, TypeOperators, UndecidableInstances #-}

module SigDesc where

import           Control.DeepSeq
import           Data.IORef
import           Data.Kind
import           Data.Maybe
import           Data.Void
import           GHC.TypeLits
import           Unsafe.Coerce                  ( unsafeCoerce )

type Exp a = a -> Type
type family Eval (e :: Exp a) :: a

infixr 3 <>
data (<>) a b

infixr 5 `P`
data SD a where
  N ::SD Void
  C ::a -> SD a
  S ::a -> SD a
  E ::a -> SD a
  P ::SD a -> SD b -> SD (a <> b)

-- data Dep (i :: SD p) (o :: SD q) where
--   Dep         ::Dep i o
--   NoDep       ::Dep i o
--   CrossDepOut ::Dep i p -> Dep i q -> Dep i (p `P` q)
--   CrossDepIn  ::Dep p o -> Dep q o -> Dep (p `P` q) o

-- crossDep
--   :: Dep i1 o1
--   -> Dep i2 o1
--   -> Dep i1 o2
--   -> Dep i2 o2
--   -> Dep (i1 `P` i2) (o1 `P` o2)
-- crossDep i1o1 i2o1 i1o2 i2o2 =
--   CrossDepOut (CrossDepIn i1o1 i2o1) (CrossDepIn i1o2 i2o2)


infixr 5 `CP`
data SDRepr (f :: SD Type -> Exp Type) (sd :: SD k) :: Type where
  CN ::SDRepr f N
  CC ::Eval (f (C a)) -> SDRepr f (C a)
  CS ::Eval (f (S a)) -> SDRepr f (S a)
  CE ::Eval (f (E a)) -> SDRepr f (E a)
  CP ::SDRepr f p -> SDRepr f q -> SDRepr f (p `P` q)

eqSDRef :: SDRef a -> SDRef b -> Bool
eqSDRef CN         CN         = True
eqSDRef (CC r1   ) (CC r2   ) = r1 == unsafeCoerce r2
eqSDRef (CS r1   ) (CS r2   ) = r1 == unsafeCoerce r2
eqSDRef (CE r1   ) (CE r2   ) = r1 == unsafeCoerce r2
eqSDRef (CP l1 r1) (CP l2 r2) = l1 `eqSDRef` l2 && r1 `eqSDRef` r2
eqSDRef _          _          = False

selLeft :: SDRepr f (a `P` b) -> SDRepr f a
selLeft (x `CP` _) = x

selRight :: SDRepr f (a `P` b) -> SDRepr f b
selRight (_ `CP` y) = y

instance NFData (SDRepr f N) where
  rnf CN = ()

instance NFData (Eval (f (C a))) => NFData (SDRepr f (C a)) where
  rnf (CC b) = rnf b

instance NFData (Eval (f (S a))) => NFData (SDRepr f (S a)) where
  rnf (CS b) = rnf b

instance NFData (Eval (f (E a))) => NFData (SDRepr f (E a)) where
  rnf (CE b) = rnf b

instance (NFData (SDRepr f a), NFData (SDRepr f b)) => NFData (SDRepr f (a `P` b)) where
  rnf (CP x y) = case (rnf x, rnf y) of
    ((), ()) -> ()

data Id a :: Exp b
type instance Eval (Id (C a)) = a
type instance Eval (Id (S a)) = (Bool, a)
type instance Eval (Id (E a)) = Maybe a
type SDVal = SDRepr Id

data Ref a :: Exp b
type instance Eval (Ref sd) = IORef (Eval (Id sd))
type SDRef = SDRepr Ref

data DependsOn a = NoDep | DepOld a | DepNew a

data Dep a :: Exp b
type instance Eval (Dep sd) = DependsOn (Eval (Ref sd))
type SDDep = SDRepr Dep

data Produce a = NoProd | Prod a

data Prod a :: Exp b
type instance Eval (Prod sd) = Produce (Eval (Ref sd))
type SDProd = SDRepr Prod

-- data Dep a :: Exp b
-- type instance Eval (Dep sd) = Bool

instance Show (SDVal N) where
  show CN = "CN"

instance Show a => Show (SDVal (E a)) where
  show (CE x) = "CE " ++ show x

instance Show a => Show (SDVal (S a)) where
  show (CS x) = "CS " ++ show x

instance Show a => Show (SDVal (C a)) where
  show (CC x) = "CC " ++ show x

instance (Show (SDVal a), Show (SDVal b)) => Show (SDVal (a `P` b)) where
  show (x `CP` y) = "(" ++ show x ++ " <> " ++ show y ++ ")"

newSDRef :: SDVal s -> IO (SDRef s)
newSDRef CN       = pure CN
newSDRef (CC cv ) = CC <$> newIORef cv
newSDRef (CS sv ) = CS <$> newIORef sv
newSDRef (CE ev ) = CE <$> newIORef ev
newSDRef (CP l r) = CP <$> newSDRef l <*> newSDRef r

readSDRef :: SDRef s -> IO (SDVal s)
readSDRef CN       = pure CN
readSDRef (CC cr ) = CC <$> readIORef cr
readSDRef (CS sr ) = CS <$> readIORef sr
readSDRef (CE er ) = CE <$> readIORef er
readSDRef (CP l r) = CP <$> readSDRef l <*> readSDRef r

writeSDRef :: SDRef s -> SDVal s -> IO ()
writeSDRef ref val = case (ref, val) of
  (CN      , CN      ) -> pure ()
  (CC ref  , CC val  ) -> writeIORef ref val
  (CS ref  , CS val  ) -> writeIORef ref val
  (CE ref  , CE val  ) -> writeIORef ref val
  (CP r1 r2, CP v1 v2) -> writeSDRef r1 v1 >> writeSDRef r2 v2

type family Raw (sd :: SD Type) :: Type where
  Raw (C a) = a
  Raw (S a) = a
  Raw (E a) = Maybe a

class IsSD sd where
  emptyRepr :: Maybe String -> SDVal sd

instance IsSD N where
  emptyRepr _ = CN

instance IsSD (C a :: SD Type) where
  emptyRepr err = CC (maybe undefined error err)

instance IsSD (S a :: SD Type) where
  emptyRepr err = CS (maybe undefined error err)

instance IsSD (E a :: SD Type) where
  emptyRepr err = CE (maybe undefined error err)

instance (IsSD i, IsSD j) => IsSD (i `P` j) where
  emptyRepr err = CP (emptyRepr err) (emptyRepr err)

class ToSD sd where
  toSD :: Raw sd -> SDRepr Id sd

instance ToSD (C a :: SD Type) where
  toSD = CC

instance ToSD (S a :: SD Type) where
  toSD val = CS (True, val)

instance ToSD (E a :: SD Type) where
  toSD = CE
