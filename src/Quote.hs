{-# LANGUAGE FlexibleContexts, OverloadedLists, RecordWildCards,
             TemplateHaskell, TupleSections, ViewPatterns, LambdaCase #-}

module Quote (sigFun, sf, quoteFun) where

import           Control.Applicative        (Alternative (..))
import           Control.Arrow              (first, second)
import           Control.Monad.Except
import           Control.Monad.State
import           Control.Monad.Writer       hiding (First)
import           Data.Foldable
import           Data.List                  (elemIndex)
import           Data.Map                   (Map)
import qualified Data.Map                   as M
import           Data.Maybe
import           Data.Sequence              (Seq (..))
import           Data.Set                   (Set)
import Control.Exception.Base (assert)
import qualified Data.Set                   as S
import qualified Data.Tree                  as T
import qualified Language.Haskell.Meta      as H
import qualified Language.Haskell.TH.Quote  as H
import qualified Language.Haskell.TH.Syntax as H
import qualified Language.Haskell.TH.FreeVariables as H

import qualified ClassSF                    as CSF
import           Lexer
import           Parser
import ComputeRouter
import qualified Router                     as CSF
import           Syntax
import  TypLexer
import  TypParser

import Debug.Trace

---------------------------------------------------------------------------
-- Template Haskell utilities
---------------------------------------------------------------------------

lambdaOfNames :: [H.Name] -> H.Exp -> H.Exp
lambdaOfNames [] body = body
lambdaOfNames names@(_ : _) body =
  H.LamE (fmap H.VarP names) body

--------------------------------------------------------------------------
-- Scope checking
---------------------------------------------------------------------------

type ParsedSig = Sig UndesVar
type ParsedStatement = Statement UndesVar (Var String)
type ParsedFun = Fun UndesVar (Var String)

type ScopedSig = Sig (Var Ident)
type ScopedStatement = Statement (Var Ident) (Var Ident)
type ScopedFun = Fun (Var Ident) (Var Ident)

type ScopeM = StateT Int (ExceptT Err H.Q)

type Subst = Map String Ident

fresh :: MonadState Int m => String -> m Ident
fresh source = do
  idx <- get
  put (idx + 1)
  pure (Ident (Just source) idx)

freshVar :: MonadState Int m => String -> m (Var Ident)
freshVar source = do
  ident <- fresh source
  pure (Var ident Nothing)

substOfSig :: Sig (Var String)
           -> ScopeM (Subst, ScopedSig)
substOfSig = go []
  where go :: Subst
           -> Sig (Var String)
           -> ScopeM (Subst, ScopedSig)
        go subst (Sig Null) = pure ([], Sig Null)
        go subst (Sig (Var id ann)) =
          case M.lookup id subst of
            Nothing -> do
              ident <- fresh "dummy"
              pure (M.insert id ident subst, Sig (Var ident ann))
            Just _ -> throwError (BoundTwice id)
        go subst (Vec l r) = do
          (subst, l) <- go subst l
          (subst, r) <- go subst r
          pure (subst, Vec l r)

        -- go subst (Sig (SigHsExp hsExp)) = do

applySubst :: Subst
           -> Sig UndesVar
           -> ScopeM (ScopedSig, Seq ScopedStatement)
applySubst _ (Sig (SigVar Null)) = pure (Sig Null, [])
applySubst subst (Sig (SigVar (Var id ann))) =
  case M.lookup id subst of
    Nothing -> throwError (NotInScope id)
    Just id -> pure (Sig (Var id ann), [])
applySubst subst (Sig (SigHsExp hsExp)) = do
  let idMap = M.toList (M.mapKeys H.mkName subst `M.restrictKeys` H.freeVars hsExp)
      names = fmap fst idMap
      scopedSigs = fmap (\(_, id) -> Var id Nothing) idMap
      fun = lambdaOfNames names hsExp
  funIdent <- fresh "fun"
  let funIdSig = Var funIdent Nothing
  funExp <- lift $ lift [|CSF.constA $(pure fun)|]
  let funStmt =
        Statement { stmtInput = Sig Null
                  , stmtOutput = Sig funIdSig
                  , stmtBody = funExp
                  }
  (outputId, statements) <-
    foldM (\(inputId, statements) sig -> do
              outputIdent <- fresh "arg"
              let outputId = Var outputIdent Nothing
              stmtBody <- lift $ lift [|CSF.arr2 ($)|]
              let statement =
                    Statement { stmtInput = Vec (Sig inputId) (Sig sig)
                              , stmtOutput = Sig outputId
                              , stmtBody = stmtBody
                              }
              pure (outputId, statements :|> statement)) (funIdSig, [funStmt])
    scopedSigs
  pure (Sig outputId, statements)
applySubst subst (Vec l r) = do
  (l, ss1) <- applySubst subst l
  (r, ss2) <- applySubst subst r
  pure (Vec l r, ss1 <> ss2)

scopeStatement :: Subst
               -> ParsedStatement
               -> ScopeM (Subst, Seq ScopedStatement)
scopeStatement oldSubst Statement { .. } = do
  (stmtInput, newStatements) <- applySubst oldSubst stmtInput
  (newSubst, stmtOutput) <- substOfSig stmtOutput
  let subst = newSubst `M.union` oldSubst
  pure (subst, newStatements :|> Statement { .. })

scopeCheck :: ParsedFun -> H.Q (Either Err ScopedFun)
scopeCheck fun = runExceptT (evalStateT (go fun) 0)
  where go Fun { funInput = input
               , funOutput = output
               , funBody = body
               } = do
          (subst, input) <- substOfSig input
          (subst, body) <-
            foldM (\(subst, body) stmt -> do
                      (subst, stmts) <- scopeStatement subst stmt
                      pure (subst, body <> stmts)) (subst, []) body
          (output, outStatements) <- applySubst subst output
          pure Fun { funInput = input
                   , funOutput = output
                   , funBody = body <> outStatements
                   }

---------------------------------------------------------------------------
-- Desugaring to TH
---------------------------------------------------------------------------

routerExp :: Router -> H.Q H.Exp
routerExp = routerExp' . simplifyRouter
  where routerExp' Id              = [| CSF.Id |]
        routerExp' Fst             = [| CSF.Fst |]
        routerExp' Snd             = [| CSF.Snd |]
        routerExp' UncancelFst     = [| CSF.UncancelFst |]
        routerExp' UncancelSnd     = [| CSF.UncancelSnd |]
        routerExp' Drop            = [| CSF.Drop |]
        routerExp' RightRot        = [| CSF.RightRot |]
        routerExp' LeftRot         = [| CSF.LeftRot |]
        routerExp' Swap            = [| CSF.Swap |]
        routerExp' Dup             = [| CSF.Dup |]
        routerExp' (First r)       = [| CSF.First $(routerExp r) |]
        routerExp' (Second r)      = [| CSF.Second $(routerExp r) |]
        routerExp' (AndThen r1 r2) = [| CSF.Compose $(routerExp r1) $(routerExp r2) |]

internalError :: a
internalError = error "It would appear something impossible happend…"

hasTypeAnn :: Sig (Var id) -> Bool
hasTypeAnn (Sig Null) = False
hasTypeAnn (Sig (Var _ ann)) = isJust ann
hasTypeAnn (Vec p1 p2) = hasTypeAnn p1 || hasTypeAnn p2

typeOfAnn :: Ann -> H.Q H.Type
typeOfAnn C = [t|'CSF.C|]
typeOfAnn S = [t|'CSF.S|]
typeOfAnn E = [t|'CSF.E|]

typeOfSig :: Sig (Var id) -> H.Q H.Type
typeOfSig (Sig Null) = [t|'CSF.N|]
typeOfSig (Sig (Var _ Nothing)) = [t|_|]
typeOfSig (Sig (Var _ (Just ann))) = [t| $(typeOfAnn ann) _ |]
typeOfSig (Vec p1 p2) = [t| 'CSF.P $(typeOfSig p1) $(typeOfSig p2)|]

joinSignals :: Foldable f => f (Sig id) -> Sig id
joinSignals = foldr1 (\sig sigs -> Vec sig sigs)

quoteFun :: ParsedFun -> H.Q H.Exp
quoteFun fun = do
  scpFun <- scopeCheck fun
  case scpFun of
    Left err         -> fail (show err)
    Right fun@Fun { .. } -> go funInput funOutput (groupStatements funBody)
  where go inputSig finalSig Empty =
          [| CSF.Router $(routerExp (matchSigs inputSig finalSig)) |]
        go inputSig finalSig (Empty :<| stmts) = go inputSig finalSig stmts
        -- For each block of equations we:
        --
        --   1. Start by filtering out of the inputSig the signals
        --   that are not necessary.
        --
        --   2. Check if the current statement uses any signal at
        --   all.
        --
        --     2.a. If it does not use any input, use UncancelFst to
        --     create a tree with this shape:
        --
        --                 /\
        --                /  \
        --           input    N
        --
        --          Using ***, pass the input unchanged and give the
        --     Null valued signal to the current statement.
        --
        --     2.b Dispatch the input with &&& to two functions. On
        --     the left, filter the inputs that are not used by any
        --     other statement following the one we are treating; on
        --     the right prepare the input for the function.
        --
        --     2.c If no one uses the current inputs after this step,
        --     we can simply discard them and avoid dispatching
        --     anything (and that avoids encountering a bug when
        --     trying to reduce a tree with no Null nodes to the Null
        --     tree in reduceTree).
        go inputSig finalSig groupedStatements@(currentGroup :<| nextGroups) =
          [| CSF.Router $(routerExp filteringRouter)
                CSF.:>>>:
             $(mainBlock)
                CSF.:>>>:
             $(go outputSig finalSig nextGroups)
           |]
          where mkExps (e1 :<| Empty) = [| $(pure e1) |]
                mkExps (e1 :<| es)    = [| $(pure e1) CSF.:***: $(mkExps es) |]

                joinedBodies = [| $(mkExps (fmap stmtBody currentGroup)) |]

                -- N(1..σ)
                allNecessaryVariables =
                  joinSignals (fmap stmtInput (join groupedStatements) :|> finalSig)

                --N(2..σ)
                allNecessaryVariablesAfter =
                  joinSignals (fmap stmtInput (join nextGroups) :|> finalSig)

                -- Step 1
                (detagSig -> filteredInput, filteringRouter) =
                  reduceTree (unitaryIndexMap (tagSig allNecessaryVariables)) inputSig

                -- Step 2

                currentGroupUsesVariables =
                  not (M.null (sigVars currGroupInputSig))

                isSomeInputUseful =
                  not $ M.null (sigVars inputSig `M.intersection`
                                sigVars allNecessaryVariablesAfter)


                (dispatchRouter, dispatchedInput)
                  | currentGroupUsesVariables = (Dup, filteredInput)
                  | otherwise = (UncancelFst, Sig Null)

                sigFunGlue = matchSigs dispatchedInput currGroupInputSig

                (filteredContinuingOutput, continuingRouter)
                  | not currentGroupUsesVariables = (filteredInput, Id)
                  | otherwise =
                      let (filteredContinuingOutput, continuingRouter) =
                            reduceTree (unitaryIndexMap (tagSig allNecessaryVariablesAfter)) filteredInput
                      in (detagSig filteredContinuingOutput, continuingRouter)

                currGroupInputSig = joinSignals (fmap stmtInput currentGroup)
                currGroupOutputSig = joinSignals (fmap stmtOutput currentGroup)

                (outputSig, mainBlock)
                  | isSomeInputUseful =
                    ( Vec filteredContinuingOutput currGroupOutputSig
                    , [| CSF.Router $(routerExp dispatchRouter)
                             CSF.:>>>:
                         ( CSF.Router $(routerExp continuingRouter)
                              CSF.:***:
                           (CSF.Router $(routerExp sigFunGlue) CSF.:>>>: $(sigFunExp))
                         )
                       |]
                    )
                  | otherwise =
                    ( currGroupOutputSig
                    , [| CSF.Router $(routerExp sigFunGlue) CSF.:>>>: $(sigFunExp) |]
                    )

                sigFunExp :: H.Q H.Exp
                sigFunExp =
                  if hasTypeAnn currGroupInputSig || hasTypeAnn currGroupOutputSig
                  then [|$(joinedBodies) ::
                         (CSF.SF $(typeOfSig currGroupInputSig)
                                 $(typeOfSig currGroupOutputSig))|]
                  else [|$(joinedBodies)|]

---------------------------------------------------------------------------
-- Type from a signal
---------------------------------------------------------------------------

quoteSig :: Sig (Maybe (H.Type, Ann)) -> H.Q H.Type
quoteSig (Sig Nothing) = [t|'CSF.N|]
quoteSig (Sig (Just (typ, ann))) = [t| $(typeOfAnn ann) $(pure typ) |]
quoteSig (Vec left right) = [t|'CSF.P $(quoteSig left) $(quoteSig right)|]

quoteTyp :: String -> H.Q H.Type
quoteTyp str =
  case parseTyp =<< scanTyp str of
    Left err -> fail err
    Right sig -> quoteSig sig

sigFun :: H.QuasiQuoter
sigFun = H.QuasiQuoter { H.quoteExp = quoteExp
                       , H.quoteType = quoteTyp
                       }
  where quoteExp str =
          case parseFun =<< scanner str of
            Left err -> fail err
            Right fun -> quoteFun fun

sf :: H.QuasiQuoter
sf = sigFun

sd = H.QuasiQuoter { H.quoteType = H.quoteType sigFun }
