{-# LANGUAGE Arrows #-}

module YampaTest where

import Control.DeepSeq
import Data.IORef
import FRP.Yampa

import Debug.Trace

type Bit = Bool

orGate :: SF (Bit, Bit) Bit
orGate = arr (uncurry (||))

xorGate :: SF (Bit, Bit) Bit
xorGate = arr (\(b1, b2) -> (b1 || b2) && not (b1 && b2))

andGate :: SF (Bit, Bit) Bit
andGate = arr (uncurry (&&))

halfAdder :: SF (Bit, Bit) (Bit, Bit)
halfAdder = proc input -> do
  output <- xorGate -< input
  carry <- andGate -< input
  returnA -< (output, carry)

fullAdder :: SF (Bit, Bit, Bit) (Bit, Bit)
fullAdder = proc (i1, i2, c0) -> do
  (s1, c1) <- halfAdder -< (i1, i2)
  (sum, c2) <- halfAdder -< (s1, c0)
  cout <- orGate -< (c2, c0)
  returnA -< (sum, cout)

fourBitAdder :: SF ((Bit, Bit, Bit, Bit), (Bit, Bit, Bit, Bit))
                ((Bit, Bit, Bit, Bit), Bit)
fourBitAdder = proc ((a0, a1, a2, a3), (b0, b1, b2, b3)) -> do
  (r0, c1) <- fullAdder -< (a0, b0, False)
  (r1, c2) <- fullAdder -< (a1, b1, c1)
  (r2, c3) <- fullAdder -< (a2, b2, c2)
  (r3, c4) <- fullAdder -< (a3, b3, c3)
  returnA -< ((r0, r1, r2, r3), c4)

type Pos = Double
type Vel = Double
type Ball = (Pos, Vel)

fallingBall :: Ball -> SF () Ball
fallingBall (y0, v0) = proc () -> do
  v <- arr (v0 +) <<< integral -< -9.81
  y <- arr (y0 +) <<< integral -< v
  returnA -< (y,v)

bouncingBall :: Ball -> SF () Ball
bouncingBall init = switch (go init) bouncingBall
  where go init = proc () -> do
          ball@(y, v) <- fallingBall init -< ()
          let evt = if y <= 0 then Event (abs y, -v) else NoEvent
          returnA -< (ball, evt)

runFor :: NFData o
       => Int
       -> SF i o
       -> IO i
       -> IO ()
runFor n sf getInput = do
  countRef <- newIORef n
  reactimate getInput
             (\_ -> do
                 input <- getInput
                 pure (0.1, Just input))
             (\_ output ->
                case rnf output of
                  () -> do
                    count <- readIORef countRef
                    if count == 0
                      then pure True
                      else do
                      writeIORef countRef (count - 1)
                      pure False)
             sf

-- testAdder :: IO ()
-- testAdder = do
--   let get4Bits = do
--         b0 <- randomIO
--         b1 <- randomIO
--         b2 <- randomIO
--         b3 <- randomIO
--         pure (b0, b1, b2, b3)
--       getInput = do
--         o1 <- get4Bits
--         o2 <- get4Bits
--         pure (o1, o2)
--   runFor 1000000 fourBitAdder getInput

-- testBounce :: IO ()
-- testBounce =
--   runFor 10000000 (bouncingBall (1, 0)) (pure ())

-- main :: IO ()
-- main = testBounce
