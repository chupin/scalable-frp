{-# LANGUAGE BangPatterns, DataKinds, EmptyCase, FlexibleContexts,
             FlexibleInstances, FunctionalDependencies, GADTs, KindSignatures,
             MultiParamTypeClasses, PartialTypeSignatures, PolyKinds,
             RankNTypes, RecursiveDo, ScopedTypeVariables, TypeFamilies,
             TypeInType, TypeOperators, UndecidableInstances #-}

module ClassSF
  ( module ClassSF
  , module Router
  , module SigDesc
  ) where

import qualified Control.Arrow                 as A
import           Control.Monad.Identity
import           Control.Monad.Reader
import           Data.IORef
import           Data.Kind                      ( Type )
import           Data.Proxy
import           Data.Void

import           Control.Category        hiding ( (.)
                                                , (>>>)
                                                )
import qualified Control.Category              as C
import           Control.DeepSeq
import           Data.Maybe
import           GHC.TypeLits                   ( ErrorMessage(..)
                                                , TypeError
                                                )

import           Debug.Trace

import           Router
import           SigDesc
import           Stepper

---------------------------------------------------------------------------
-- Compilation
---------------------------------------------------------------------------

type family Not (b :: Bool) where
  Not 'True  = 'False
  Not 'False = 'True

data Opt pst typ where
  None ::Opt 'False typ
  Some ::typ -> Opt 'True typ

instance Functor (Opt pst) where
  fmap _ None     = None
  fmap f (Some x) = Some (f x)

apOpt :: Opt pst (a -> b) -> Opt pst a -> Opt pst b
apOpt None     None     = None
apOpt (Some f) (Some x) = Some (f x)

data OptRef b sd :: Exp c
type instance Eval (OptRef b sd) = Opt b (Eval (Ref sd))

type OptSDRef b o = SDRepr (OptRef b) o

newtype Context = Context
  { timeRef :: IORef Double
  }

data Compiled (i :: SD k) (o :: SD k') where
  Compiled   ::(forall b. SDRef i -> Opt b (SDRef o) -> ReaderT Context IO (Opt (Not b) (SDRef o), Stepper))
             -> Compiled i o

  Route      ::Router i o
             -> Compiled i o

routeStepper :: Router i o -> SDRef i -> SDRef o -> Stepper
routeStepper rr ir or = FStpr (readSDRef ir >>= writeSDRef or . route rr)

unwrapCompiled
  :: Compiled i o
  -> SDRef i
  -> Opt b (SDRef o)
  -> ReaderT Context IO (Opt (Not b) (SDRef o), Stepper)
unwrapCompiled csf ir or = case (csf, or) of
  (Route    rr     , None   ) -> pure (Some (route rr ir), Done)
  (Route    rr     , Some or) -> pure (None, routeStepper rr ir or)
  (Compiled befStpr, _      ) -> befStpr ir or

---------------------------------------------------------------------------
-- Compilation
---------------------------------------------------------------------------

data AbsRef where
  AbsRef ::IORef k -> AbsRef

data Mem k = New k | Old k

data PartCompiled i o = PartCompiled [Mem AbsRef] Stepper [AbsRef]

-- data DepCo = DepCo
--   { reads   :: ()
--   , writes  :: ()
--   , stepper :: Stepper
--   }

data Delaying = Delay | NoDelay

data PrimSF (i :: SD p) (o :: SD q) where
  Jump ::s -> (s -> a -> (Maybe b, s)) -> PrimSF (S a) (E b)
  Edge ::s -> (s -> a -> (Maybe b, s)) -> PrimSF (C a) (E b)
  AccumBy ::s -> (s -> a -> s) -> PrimSF (E a) (S s)

  Arr ::(Raw (k i) -> Raw (k o)) -> PrimSF (k i) (k o)
  Arr2 ::IsSD (k c)
       => (Raw (k a) -> Raw (k b) -> Raw (k c))
       -> PrimSF (k a `P` k b) (k c)
  Tag ::(a -> b -> c)
      -> PrimSF (E a `P` C b) (E c)

  Switch ::IsSD b
         => SF a (E c `P` b)
         -> (c -> SF a b)
         -> PrimSF a b
  Loop ::IsSD c => SF (a `P` c) (b `P` c) -> PrimSF a b

infixr 1 :>>>:
infixr 3 :***:

data SF (i :: SD p) (o :: SD q) where
  Router ::Router i o -> SF i o
  (:>>>:) ::SF a b -> SF b c -> SF a c
  (:***:) ::SF a b -> SF c d -> SF (a `P` c) (b `P` d)
  PrimSF ::PrimSF i o -> SF i o
  Const ::SDVal o -> SF i o
  Integr ::SF (C Double) (C Double)

withOpt
  :: (MonadIO m, IsSD o)
  => Opt b (SDRef o)
  -> (SDRef o -> k)
  -> m (Opt (Not b) (SDRef o), k)
withOpt None fun = do
  or <- liftIO $ newSDRef (emptyRepr Nothing)
  let !val = fun or
  pure (Some or, val)
withOpt (Some or) fun = do
  let !val = fun or
  pure (None, val)

compilePrim
  :: PrimSF i o
  -> SDRef i
  -> Opt b (SDRef o)
  -> ReaderT Context IO (Opt (Not b) (SDRef o), Stepper)
compilePrim (Jump init jump) (CS ir) or = do
  pref <- lift $ newIORef init
  withOpt or $ \(CE or) -> FStpr $ do
    (chg, nval) <- readIORef ir
    if chg
      then do
        oval <- readIORef pref
        let !(evt, stVal) = jump oval nval
        writeIORef pref stVal
        writeIORef or   evt
      else writeIORef or Nothing
compilePrim (Edge init edge) ir or = do
  pref <- lift $ newIORef init
  withOpt or $ \(CE or) -> FStpr $ do
    CC nv <- readSDRef ir
    prev  <- readIORef pref
    let !(evt, ns) = edge prev nv
    writeIORef or   evt
    writeIORef pref ns
compilePrim (AccumBy init accum) ir or = do
  (mor, or) <- case or of
    None -> do
      or <- lift $ newIORef (True, init)
      pure (Some (CS or), or)
    Some (CS or) -> do
      lift $ writeIORef or (True, init)
      pure (None, or)
  pure
    ( mor
    , FStpr $ do
      CE evt <- readSDRef ir
      case evt of
        Nothing -> modifyIORef or (\(_, val) -> (False, val))
        Just nv -> modifyIORef or (\(_, val) -> (True, accum val nv))
    )
compilePrim (Arr arr) ir or = mkStpr arr ir or
 where
  mkStpr
    :: forall k i o b
     . (Raw (k i) -> Raw (k o))
    -> SDRef (k i)
    -> Opt b (SDRef (k o))
    -> ReaderT Context IO (Opt (Not b) (SDRef (k o)), Stepper)
  mkStpr arr ir or = case or of
    Some or -> pure (None, FStpr (stprOfArr arr ir or))
    None    -> do
      or <- lift (newSDRef . valOfArr arr =<< readSDRef ir)
      pure (Some or, FStpr (stprOfArr arr ir or))

  valOfArr :: (Raw (k i) -> Raw (k o)) -> SDVal (k i) -> SDVal (k o)
  valOfArr fun (CC iv       ) = CC (fun iv)
  valOfArr fun (CS (chg, iv)) = CS (chg, fun iv)
  valOfArr fun (CE iv       ) = CE (fun iv)

  stprOfArr :: (Raw (k i) -> Raw (k o)) -> SDRef (k i) -> SDRef (k o) -> IO ()
  stprOfArr fun (CC ir) (CC or) = readIORef ir >>= writeIORef or . fun
  stprOfArr fun (CS ir) (CS or) = do
    (change, val) <- readIORef ir
    if change
      then writeIORef or (True, fun val)
      else modifyIORef or (A.first (const False))
  stprOfArr fun (CE ir) (CE or) = readIORef ir >>= writeIORef or . fun
compilePrim (Arr2 fun) (ir1 `CP` ir2) or = withOpt or $ \or ->
  case (ir1, ir2, or) of
    (CC ir1, CC ir2, CC or) -> FStpr $ do
      iv1 <- readIORef ir1
      iv2 <- readIORef ir2
      writeIORef or (fun iv1 iv2)
    (CS ir1, CS ir2, CS or) -> FStpr $ do
      (chg1, iv1) <- readIORef ir1
      (chg2, iv2) <- readIORef ir2
      if chg1 || chg2
        then writeIORef or (True, fun iv1 iv2)
        else modifyIORef or (\(_, oval) -> (False, oval))
    (CE ir1, CE ir2, CE or) -> FStpr $ do
      evt1 <- readIORef ir1
      evt2 <- readIORef ir2
      writeIORef or (fun evt1 evt2)
compilePrim (Tag tag) (CE ire `CP` CC irc) or = withOpt or $ \(CE or) ->
  FStpr $ do
    evt <- readIORef ire
    case evt of
      Nothing  -> writeIORef or Nothing
      Just evt -> do
        rc <- readIORef irc
        writeIORef or (Just (evt `tag` rc))
compilePrim (Switch fsf switch) ir or = do
  (out, or) <- case or of
    None -> do
      or <- lift $ newSDRef (emptyRepr Nothing)
      pure (Some or, or)
    Some or -> pure (None, or)
  evtRef       <- lift $ newIORef Nothing
  (None, stpr) <- unwrapCompiled (compile fsf) ir (Some (CE evtRef `CP` or))
  context      <- ask
  let vstpr stpr = do
        stpr <- execStepper stpr
        evt  <- readIORef evtRef
        case evt of
          Nothing  -> pure (VStpr (vstpr stpr))
          Just val -> do
            (None, !stpr) <- runReaderT
              (unwrapCompiled (compile (switch val)) ir (Some or))
              context
            execStepper stpr
  pure (out, VStpr (vstpr stpr))
compilePrim (Loop sf) _ _ = undefined

compile :: SF i o -> Compiled i o
compile (PrimSF prim              ) = Compiled $ compilePrim prim
compile (Router router            ) = Route router
compile (_         :>>>: Const c  ) = compile (Const c)
compile (Router Id :>>>: f        ) = compile f
compile (f         :>>>: Router Id) = compile f
compile (Router f  :>>>: Router g ) = compile (Router (f `Compose` g))
compile Integr                      = Compiled $ \(CC ir) or -> do
  Context { timeRef = dtRef } <- ask
  oir                         <- lift $ newIORef 0
  let updateOIR = do
        acc <- readIORef oir
        dt  <- readIORef dtRef
        iv  <- readIORef ir
        writeIORef oir $! (acc + dt * iv)
      stp1 = PartCompiled (CC (DepNew ir)) (FStpr updateOIR) (CC (Prod oir))
      stp2 or = PartCompiled (CC (DepOld oir))
                             (FStpr (readIORef oir >>= writeIORef or))
                             (CC (Prod or))
  withOpt or $ \(CC or) -> [stp1, stp2 or]
  -- let vstpr or acc = do
  --       writeIORef or acc
  --       iv <- readIORef ir
  --       dt <- readIORef dtRef
  --       let !acc' = acc + dt * iv
  --       pure (VStpr (vstpr or acc'))
  -- withOpt or $ \(CC or) -> VStpr (vstpr or 0)
  -- $ do
  --   readIORef oir >>= writeIORef or
  --   readIORef ir >>= writeIORef pir
  --   pure $ FStpr $ do
  --     oi <- readIORef oir
  --     writeIORef or oi
  --     pv <- readIORef pir
  --     dt <- readIORef dtRef
  --     let !intv = oi + dt * pv
  --     writeIORef oir intv
  --     readIORef ir >>= writeIORef pir
compile (Const val) = Compiled $ \_ or -> case or of
  None -> do
    or <- lift $ newSDRef val
    pure (Some or, Done)
  Some or -> pure (None, FStpr (writeSDRef or val))
compile (PrimSF (Arr f) :>>>: PrimSF (Arr g)) =
  Compiled $ compilePrim (Arr (g . f))
compile (sf :>>>: sg) = case (compile sf, compile sg) of
  (Route fc, Route gc) -> Route (Compose fc gc)
  (Route rr, Compiled mkStpr) -> Compiled (\ir or -> mkStpr (route rr ir) or)
  (Compiled mkStpr, Route rr) -> Compiled
    (\ir or -> do
      (Some imr, stpr1) <- mkStpr ir None
      case or of
        None    -> pure (Some (route rr imr), stpr1)
        Some or -> do
          let stpr2 = FStpr (readSDRef imr >>= writeSDRef or . route rr)
          pure (None, stpr1 `seqStepper` stpr2)
    )
  (Compiled mkStpr1, Compiled mkStpr2) -> Compiled
    (\ir or -> do
      (Some imr, stpr1) <- mkStpr1 ir None
      (or'     , stpr2) <- mkStpr2 imr or
      pure (or', stpr1 `seqStepper` stpr2)
    )
compile (sf :***: sg) = case (compile sf, compile sg) of
  (Route    rf     , Route rg        ) -> Route (pairRoute rf rg)
  (Compiled mkStpr1, Compiled mkStpr2) -> Compiled
    (\(ir1 `CP` ir2) or -> do
      (or1, stpr1) <- mkStpr1 ir1 (fmap selLeft or)
      (or2, stpr2) <- mkStpr2 ir2 (fmap selRight or)
      pure (fmap CP or1 `apOpt` or2, stpr1 `seqStepper` stpr2)
    )
  (Compiled mkStpr, Route rr) -> Compiled
    (\(ir1 `CP` ir2) or -> do
      (or1, stpr1) <- mkStpr ir1 (fmap selLeft or)
      case (or, or1) of
        (None, Some or1) -> pure (Some (or1 `CP` route rr ir2), stpr1)
        (Some (or1 `CP` or2), None) ->
          pure (None, stpr1 `seqStepper` routeStepper rr ir2 or2)
    )
  (Route rr, Compiled mkStpr) -> Compiled
    (\(ir1 `CP` ir2) or -> do
      (or2, stpr2) <- mkStpr ir2 (fmap selRight or)
      case (or, or2) of
        (None, Some or2) -> pure (Some (route rr ir1 `CP` or2), stpr2)
        (Some (or1 `CP` or2), None) ->
          pure (None, routeStepper rr ir1 or1 `seqStepper` stpr2)
    )

data Node i o = Node (SDRef i) (SDRef o) (PrimSF i o)

data GraphEdge where
  GraphEdge ::(IsSD i, IsSD o) => SDRef i -> SDRef o -> Stepper -> GraphEdge

-- edgeOfPrimSF :: PrimSF i o -> SDRef i -> SDRef o -> [GraphEdge]
-- edgeOfPrimSF (Jump    _ _) ir or = [GraphEdge ir or]
-- edgeOfPrimSF (Edge    _ _) ir or = [GraphEdge ir or]
-- edgeOfPrimSF (AccumBy _ _) ir or = [GraphEdge ir or]
-- edgeOfPrimSF (Const _    ) _  _  = []
-- edgeOfPrimSF (Arr   _    ) ir or = [GraphEdge ir or]
-- edgeOfPrimSF (Arr2  _    ) ir or = [GraphEdge ir or]
-- edgeOfPrimSF (Tag   _    ) ir or = [GraphEdge ir or]
-- edgeOfPrimSF Integr        _  _  = []
-- edgeOfPrimSF (Switch _ _)  ir or = [GraphEdge ir or]
-- edgeOfPrimSF (Loop _    )  ir or = [GraphEdge ir or]
-- Jump ::s -> (s -> a -> (Maybe b, s)) -> PrimSF (S a) (E b)
--   Edge ::s -> (s -> a -> (Maybe b, s)) -> PrimSF (C a) (E b)
--   AccumBy ::s -> (s -> a -> s) -> PrimSF (E a) (S s)

--   Const ::SDVal o -> PrimSF i o
--   Arr ::(Raw (k i) -> Raw (k o)) -> PrimSF (k i) (k o)
--   Arr2 ::IsSD (k c)
--        => (Raw (k a) -> Raw (k b) -> Raw (k c))
--        -> PrimSF (k a `P` k b) (k c)
--   Tag ::(a -> b -> c)
--       -> PrimSF (E a `P` C b) (E c)

--   Integr ::PrimSF (C Double) (C Double)
--   Switch ::IsSD b
--          => SF a (E c `P` b)
--          -> (c -> SF a b)
--          -> PrimSF a b
--   Loop ::IsSD c => SF (a `P` c) (b `P` c) -> PrimSF a b

---------------------------------------------------------------------------
-- Routing
---------------------------------------------------------------------------

identity :: SF a a
identity = Router Id

selFst :: SF (a `P` b) a
selFst = Router Fst

selSnd :: SF (a `P` b) b
selSnd = Router Snd

---------------------------------------------------------------------------
-- Combinators
---------------------------------------------------------------------------

first :: SF a b -> SF (a `P` d) (b `P` d)
first f = f :***: Router Id

second :: (IsSD a, IsSD b, IsSD d) => SF a b -> SF (d `P` a) (d `P` b)
second f = Router Swap :>>>: first f :>>>: Router Swap

infixr 1 >>>
(>>>) :: IsSD b => SF a b -> SF b c -> SF a c
f >>> g = f :>>>: g

infixl 1 <<<
(<<<) :: IsSD b => SF b c -> SF a b -> SF a c
(<<<) = flip (>>>)

infixr 3 &&&
(&&&) :: SF a b -> SF a c -> SF a (b `P` c)
f &&& g = Router Dup :>>>: f :***: g

infixr 3 ***
(***) :: SF a c -> SF b d -> SF (a `P` b) (c `P` d)
f *** g = f :***: g

---------------------------------------------------------------------------
-- Stateful manipulations
---------------------------------------------------------------------------

edge :: s -> (s -> a -> (Maybe b, s)) -> SF (C a) (E b)
edge init acc = PrimSF (Edge init acc)

edgeJust :: SF (C (Maybe a)) (E a)
edgeJust = edge False trig
 where
  trig False (Just evt) = (Just evt, True)
  trig True  (Just _  ) = (Nothing, True)
  trig _     Nothing    = (Nothing, False)

edgeBool :: SF (C Bool) (E ())
edgeBool = edge False trig
 where
  trig False True  = (Just (), True)
  trig True  True  = (Nothing, True)
  trig _     False = (Nothing, False)

edgeGuard :: SF (C Bool) (E ())
edgeGuard = edge () (\() st -> (guard st, ()))

up :: (Ord a, Fractional a) => SF (C a) (E ())
up = edge 0 (\pv nv -> (guard (pv < 0 && nv > 0), nv))

jump :: s -> (s -> a -> (Maybe b, s)) -> SF (S a) (E b)
jump = Jump

jumpBool :: SF (S Bool) (E ())
jumpBool = jump True trig
 where
  trig _     False = (Nothing, False)
  trig True  True  = (Nothing, True)
  trig False True  = (Just (), True)

jumpJust :: SF (S (Maybe a)) (E a)
jumpJust = jump True trig
 where
  trig False (Just evt) = (Just evt, True)
  trig True  (Just _  ) = (Nothing, True)
  trig _     Nothing    = (Nothing, False)

accumBy :: s -> (s -> a -> s) -> SF (E a) (S s)
accumBy = AccumBy

---------------------------------------------------------------------------
-- The basic functions
---------------------------------------------------------------------------

constA :: ToSD o => Raw o -> SF i o
constA raw = Const (toSD raw)

-- constA :: forall i o. SDVal o -> SF i o
-- constA val = Const val

---------------------------------------------------------------------------
-- Arr
---------------------------------------------------------------------------

arr :: (Raw (k a) -> Raw (k b)) -> SF (k a) (k b)
arr = Arr

arrC :: (a -> b) -> SF (C a) (C b)
arrC = Arr

arrS :: (a -> b) -> SF (S a) (S b)
arrS = Arr

arrE :: (Maybe a -> Maybe b) -> SF (E a) (E b)
arrE = Arr

---------------------------------------------------------------------------
-- Arr 2
---------------------------------------------------------------------------

arr2
  :: IsSD (k o)
  => (Raw (k p) -> Raw (k q) -> Raw (k o))
  -> SF (k p `P` k q) (k o)
arr2 = Arr2

arr2CC :: (a -> b -> c) -> SF (C a `P` C b) (C c)
arr2CC = Arr2

arr2SS :: (a -> b -> c) -> SF (S a `P` S b) (S c)
arr2SS = Arr2

arr2EE :: (Maybe a -> Maybe b -> Maybe c) -> SF (E a `P` E b) (E c)
arr2EE = Arr2

tag :: (a -> b -> c) -> SF (E a `P` C b) (E c)
tag = Tag

---------------------------------------------------------------------------
-- integral
---------------------------------------------------------------------------

integr :: SF (C Double) (C Double)
integr = Integr

time :: SF i (C Double)
time = constA 1 >>> integr

imIntegr :: Double -> SF (C Double) (C Double)
imIntegr i = integr >>> arrC (i +)

---------------------------------------------------------------------------
-- Switching
---------------------------------------------------------------------------

switch :: IsSD b => SF a (E c `P` b) -> (c -> SF a b) -> SF a b
switch sf sw = Switch sf sw

recSwitch :: IsSD b => (c -> SF a (E c `P` b)) -> c -> SF a b
recSwitch sf init = switch (sf init) (recSwitch sf)

-- dswitch :: IsSD b => SF a (E c `P` b) -> (c -> SF a b) -> SF a b
-- dswitch sf sw = Switch sf sw

-- -- dswitch :: SF a (E c `P` b) -> (c -> SF a b) -> SF a b
-- -- dswitch sf sw = SF (Switch Delay sf sw)

---------------------------------------------------------------------------
-- Execution
---------------------------------------------------------------------------

type DTime = Double

data ExecutionEngine i o = ExecutionEngine
  { context   :: Context
  , inputRef  :: SDRef i
  , outputRef :: SDRef o
  , step      :: IORef Stepper
  }

instance (NFData (SDRef i), NFData (SDRef o)) => NFData (ExecutionEngine i o) where
  rnf ExecutionEngine { inputRef = ir, outputRef = or, step = stpr } =
    case (rnf ir, rnf or) of
      ((), ()) -> rnf stpr

initializeEngine :: IsSD i => SF i o -> IO (ExecutionEngine i o)
initializeEngine sf = do
  ir      <- newSDRef (emptyRepr Nothing)
  timeRef <- newIORef 0
  let context = Context { timeRef = timeRef }
  (Some or, !stpr) <- runReaderT (unwrapCompiled (compile sf) ir None) context
  stprRef          <- newIORef stpr
  pure ExecutionEngine { context   = context
                       , inputRef  = ir
                       , outputRef = or
                       , step      = stprRef
                       }

stepEngine :: ExecutionEngine i o -> DTime -> SDVal i -> IO (SDVal o)
stepEngine ee@ExecutionEngine { inputRef = ir, outputRef = or, step = stprRef, context = context } dt val
  = do
    writeIORef (timeRef context) dt
    writeSDRef ir val
    stpr  <- readIORef stprRef
    !stpr <- execStepper stpr
    writeIORef stprRef stpr
    readSDRef or

reactimate
  :: IsSD i => IO (DTime, SDVal i) -> (SDVal o -> IO Bool) -> SF i o -> IO ()
reactimate getInput procOutput sigFun = do
  engine <- initializeEngine sigFun
  let run = do
        (dt, input) <- getInput
        output      <- stepEngine engine dt input
        stop        <- procOutput output
        unless stop $ run
  run
