{-# LANGUAGE DataKinds, FlexibleContexts, GADTs, NumericUnderscores,
             PartialTypeSignatures, QuasiQuotes, TypeInType, TypeOperators #-}

module Main where

import           Control.Monad.Identity
import           Criterion.Main

import           ClassSF
import           Control.DeepSeq
import           Quote
import           Utils
import qualified YampaTest              as Y

import           Debug.Trace

---------------------------------------------------------------------------
-- Micro benchmarks
---------------------------------------------------------------------------

type Bit = Bool

orGate :: SF (C Bit `P` C Bit) (C Bit)
orGate = arr2CC (||)

xorGate :: SF (C Bit `P` C Bit) (C Bit)
xorGate = arr2CC (\b1 b2 -> (b1 || b2) && not (b1 && b2))

andGate :: SF (C Bit `P` C Bit) (C Bit)
andGate = arr2CC (&&)

halfAdder :: SF (C Bit `P` C Bit) (C Bit `P` C Bit)
halfAdder = [sigFun| proc {i1, i2} do
                       output <- xorGate -< {i1, i2};
                       carry  <- andGate -< {i1, i2};
                       out {output, carry}
                       |]

fullAdder :: SF (C Bit `P` C Bit `P` C Bit) (C Bit `P` C Bit)
fullAdder = [sigFun| proc {i1, i2, c0} do
                       {s1, c1} <- halfAdder -< {i1, i2};
                       {sum, c2} <- halfAdder -< {s1, c0};
                       cout <- orGate -< {c2, c0};
                       out {sum, cout}
                       |]

fourBitAdder :: SF ((C Bit `P` C Bit `P` C Bit `P` C Bit) `P`
                    (C Bit `P` C Bit `P` C Bit `P` C Bit))
                ((C Bit `P` C Bit `P` C Bit `P` C Bit) `P` C Bit)
fourBitAdder = [sigFun| proc {{a0,a1,a2,a3}, {b0,b1,b2,b3}} do
                          c0 <- constA False -< {};
                          {r0, c1} <- fullAdder -< {a0,b0,c0};
                          {r1, c2} <- fullAdder -< {a1,b1,c1};
                          {r2, c3} <- fullAdder -< {a2,b2,c2};
                          {r3, c4} <- fullAdder -< {a3,b3,c3};
                          out {{r0, r1, r2, r3}, c4}
                          |]

type Pos = Double
type Vel = Double
type Ball = (Pos, Vel)
type BallSig = C Pos `P` C Vel

fallingBall :: IsSD input => Ball -> SF input BallSig
fallingBall (y0,v0) =
  [sigFun| proc input do
             negg <- constA (-9.81) -< {};
             dt <- constA 0.1 -< {};
             v <- arrC (+ v0) <<< integr -< {negg, dt};
             y <- arrC (+ y0) <<< integr -< {v, dt};
             out {y,v}
             |]

bouncingBall :: IsSD input => Ball -> SF input BallSig
bouncingBall init =
  switch go (\(y,v) -> bouncingBall (abs y, -v))
  where go = [sigFun| proc input do
                        {y,v} <- fallingBall init -< input;
                        pos <- arr2CC (,) -< {y,v};
                        negy <- arrC negate -< y;
                        evt <- Up -< negy;
                        trig <- arr2CE (<$) -< {pos,evt};
                        out {trig, {y,v}}
                        |]


-- go init = [sigFun| proc input do
--             {y,v} <- fallingBall init -< input;
--             pos <- arr2CC (,) -< {y,v};
--             negy <- arrC negate -< y;
--             evt <- Up -< negy;
--             trig <- arr2CE (<$) -< {pos,evt};
--             out {trig, {y,v}}
--             |]


foldInput :: IsSD i => SF i o -> [SDVal i] -> IO [SDVal o]
foldInput sf is = do
  ee <- initializeEngine (compile sf)
  (_, os) <-
    foldM (\(ee, os) i -> do
              (o, ee) <- stepEngine ee i
              pure (ee, o : os)) (ee, []) is
  pure (reverse os)

-- runFor :: NFData (SDVal o)
--        => Int
--        -> ExecutionEngine i o
--        -> IO (SDVal i)
--        -> IO ()
-- runFor 0 _ _ = pure ()
-- runFor n engine getInput = do
--   input <- getInput
--   (out, engine) <- stepEngine engine input
--   case rnf out of
--     () -> runFor (n - 1) engine getInput

-- testAdder :: IO ()
-- testAdder = do
--   let get4Bits = do
--         b0 <- randomIO
--         b1 <- randomIO
--         b2 <- randomIO
--         b3 <- randomIO
--         pure (CC b0 `CP` CC b1 `CP` CC b2 `CP` CC b3)
--       getInput = do
--         o1 <- get4Bits
--         o2 <- get4Bits
--         pure (o1 `CP` o2)
--   input <- getInput
--   engine <- initializeEngine (compile fourBitAdder) input
--   runFor 1000000 engine getInput
--   -- let go 0 _ = pure ()
--   --     go n engine = do
--   --       input <- getInput
--   --       (out, engine) <- stepEngine engine input
--   --       case rnf out of
--   --         () -> go (n - 1) engine
--   -- go 1000000 engine

-- testBounce :: IO ()
-- testBounce = do
--   engine <- initializeEngine (compile (bouncingBall (0, 1))) (CC ())
--   runFor 10000000 engine (pure (CC ()))

bitVal :: SDVal (C Bit `P` C Bit `P` C Bit `P` C Bit) -> Int
bitVal (CC b1 `CP` CC b2 `CP` CC b3 `CP` CC b4) =
  sum $ zipWith (\b p -> fromEnum b * p) [b1, b2, b3, b4] [1,2,4,8]

toYampa :: SDVal (C Bit `P` C Bit `P` C Bit `P` C Bit) -> (Bit, Bit, Bit, Bit)
toYampa (CC b1 `CP` CC b2 `CP` CC b3 `CP` CC b4) = (b1, b2, b3, b4)

several :: NFData a
        => Int
        -> (a -> IO a)
        -> a
        -> IO ()
several 0 _ val    = pure ()
several n func val = do
  val <- func val
  case rnf val of
    () -> several (n - 1) func val

count = 1_000_000

b1 = CC True `CP` CC False `CP` CC False `CP` CC True
b2 = CC False `CP` CC False `CP` CC True `CP` CC False

main = do
  engine <- initializeEngine (compile fourBitAdder)
  several count (\engine -> snd <$> stepEngine engine (b1 `CP` b2)) engine

-- main =
--   Y.runFor count (Y.bouncingBall (1,0)) (pure ())

-- main =
--   Y.runFor count (Y.fourBitAdder) (pure (toYampa b1, toYampa b2))


-- main :: IO ()
-- main = defaultMain benches
--   where count = 1000000
--         initBounce = initializeEngine (compile (bouncingBall (1,0)))
--         benchBounce engine =
--           bench "bounce"
--           (nfIO (several count (\engine -> snd <$> stepEngine engine (CC ())) engine))

--         b1 = CC True `CP` CC False `CP` CC False `CP` CC True
--         b2 = CC False `CP` CC False `CP` CC True `CP` CC False
--         yb1 = toYampa b1
--         yb2 = toYampa b2


--         zeroBit = CC False `CP` CC False `CP` CC False `CP` CC False
--         initAdder = initializeEngine (compile fourBitAdder)
--         benchAdder b1 b2 engine =
--           bench ("Adder " ++ show (bitVal b1) ++ " + " ++ show (bitVal b2))
--           (nfIO (several count
--                   (\engine -> snd <$> stepEngine engine (zeroBit `CP` zeroBit)) engine))

--         benches = [ bgroup "sfrp"
--                     [ env initBounce benchBounce
--                     , env initAdder (benchAdder b1 b2)
--                     ]
--                   , bgroup "yampa"
--                     [ bench "bounce" (nfIO (Y.runFor count (Y.bouncingBall (1,0)) (pure ())))
--                     , bench "adder" (nfIO (Y.runFor count Y.fourBitAdder (pure (yb1, yb2))))
--                     ]

--                   ]
