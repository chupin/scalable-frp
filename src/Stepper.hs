{-# LANGUAGE BangPatterns #-}

module Stepper where

import Control.DeepSeq

data Stepper = Done
             | FStpr !(IO ())
             | VStpr !(IO Stepper)

instance NFData Stepper where
  rnf Done       = ()
  rnf (FStpr !_) = ()
  rnf (VStpr !_) = ()

-- Sequences two steppers. Automatically simplifies VStpr evolving to
-- FStpr or to Done.
seqStepper :: Stepper -> Stepper -> Stepper
seqStepper Done s2               = s2
seqStepper s1 Done               = s1
seqStepper (FStpr s1) (FStpr s2) = FStpr (s1 >> s2)
seqStepper (FStpr s1) (VStpr s2) = VStpr $ do
  () <- s1
  !s2 <- s2
  pure (FStpr s1 `seqStepper` s2)
seqStepper (VStpr s1) (FStpr s2) = VStpr $ do
  !s1 <- s1
  () <- s2
  pure (s1 `seqStepper` FStpr s2)
seqStepper (VStpr s1) (VStpr s2) = VStpr $ do
  !s1 <- s1
  !s2 <- s2
  pure (s1 `seqStepper` s2)

execStepper :: Stepper -> IO Stepper
execStepper Done                 = pure Done
execStepper stepper@(FStpr stpr) = stpr >> pure stepper
execStepper (VStpr stpr)         = stpr
