{

module Parser (parseFun) where

import Syntax
import Lexer
import qualified Language.Haskell.Meta      as H
import Data.Sequence (Seq(..))
import Data.Maybe

}

%name parseFun Fun

%monad { Either String }
%tokentype { Tok }

%token
        'proc'          { TokProc }
        'do'            { TokDo }
        'out'           { TokOut }
        '::'            { TokDoubleCol }
        ';'             { TokSemiCol }
        ','             { TokComma }
        expr            { TokHsExpr $$ }
        liftedExpr      { TokLiftHsExpr $$ }
        var             { TokIdent $$ }
        ann             { TokAnn $$ }
        '{'             { TokLBrace }
        '}'             { TokRBrace }

%%

AnnVar(Var): Var '::' ann { ($1, $3) }

OptAnnVar(Var): var { ($1, Nothing) }
              | var '::' ann { ($1, Just $3) }

Sig(Var): '{' '}' { Sig Nothing }
        | Var { Sig (Just $1) }
        | '{' Sig(Var) '}' { $2 }
        | Vec(Sig(Var)) { $1 }

HsExp: expr             {% H.parseExp $1 }
LiftedHsExp: liftedExpr {% H.parseExp $1 }

Vec(Sig): '{' Sig ',' Sig '}' { Vec $2 $4 }

StmtList: { Empty }
        | Stmt ';' StmtList { $1 :<| $3 }

BaseSig: OptAnnVar(var) { SigVar (Var (fst $1) (snd $1)) }
       | LiftedHsExp    { SigHsExp $1 }

Stmt:: { Statement UndesVar (Var String) }
Stmt: Sig(OptAnnVar(var)) HsExp Sig(BaseSig) {
  Statement { stmtInput = fmap toSig $3
            , stmtOutput = fmap toVar $1
            , stmtBody = $2
            } }

Fun: 'proc' Sig(OptAnnVar(var)) 'do' StmtList 'out' Sig(BaseSig)
      { Fun { funInput = fmap (maybe Null (uncurry Var)) $2 --fmap  $2
            , funBody = $4
            , funOutput = fmap (fromMaybe (SigVar Null)) $>
            } }

{

toVar Nothing = Null
toVar (Just (v, a)) = Var v a

toSig = fromMaybe (SigVar Null)



happyError toks = Left ("Parse error on " ++ show toks)

}
