{-# LANGUAGE DataKinds, GADTs, KindSignatures, PolyKinds, TypeInType,
             TypeOperators, RankNTypes #-}

module Router where

import           SigDesc

data Router (i :: SD k) (o :: SD k') where
  Fst         ::Router (i `P` i')        i
  Snd         ::Router (i `P` i')        i'
  UncancelFst ::Router i                 (i `P` N)
  UncancelSnd ::Router i                 (N `P` i)
  Drop        ::Router i                 N
  RightRot    ::Router ((a `P` b) `P` c) (a `P` (b `P` c))
  LeftRot     ::Router (a `P` (b `P` c)) ((a `P` b) `P` c)
  Swap        ::Router (a `P` b)         (b `P` a)
  Dup         ::Router a                 (a `P` a)

  Id          ::Router a                 a

  First       ::Router i o
              -> Router (i `P` i')        (o `P` i')
  Second      ::Router i o
              -> Router (i' `P` i)        (i' `P` o)

  Compose     ::Router a b
              -> Router b c
              -> Router a c

pairRoute :: Router a b -> Router c d -> Router (a `P` c) (b `P` d)
pairRoute rf rg = First rf `Compose` Second rg

distributeRoute :: Router a b -> Router a c -> Router a (b `P` c)
distributeRoute rf rg = Dup `Compose` pairRoute rf rg

route :: Router i o -> SDRepr f i -> SDRepr f o
route Fst           (x `CP` _)                   = x
route Snd           (_ `CP` y)                   = y
route UncancelFst   x                            = x `CP` CN
route UncancelSnd   x                            = CN `CP` x
route Drop          _                            = CN
route RightRot      ((x `CP` y) `CP` z         ) = x `CP` (y `CP` z)
route LeftRot       (x          `CP` (y `CP` z)) = (x `CP` y) `CP` z
route Swap          (x          `CP` y         ) = y `CP` x
route Dup           x                            = x `CP` x
route Id            x                            = x
route (First  f   ) (x `CP` y)                   = route f x `CP` y
route (Second f   ) (x `CP` y)                   = x `CP` route f y
route (Compose f g) r                            = route g (route f r)
