{-# LANGUAGE Arrows, BangPatterns, CPP, DataKinds, GADTs, TemplateHaskell #-}

module Main where

import qualified ClassSF
import           Control.DeepSeq    hiding (force)
import           Control.Monad
import           Criterion.Main
import           Data.IORef
import qualified FRP.Yampa          as Yampa
import qualified GHC.Classes
import qualified GHC.Num
import qualified Router
import           System.Environment
import           System.Random

import           Debug.Trace

#include "/tmp/Lol.hs"

up :: Yampa.SF Double (Yampa.Event ())
up = Yampa.arr (\val -> guard (val >= 0)) --Yampa.edgeBy (\pv nv -> let a = guard (pv < 0 && nv > 0) in traceShow a a) 0

#include "/tmp/LolYampa.hs"

stepCount :: Int
stepCount = 100000

mkInputs :: RandomGen g => g -> [Double]
mkInputs g = i : mkInputs g'
  where (i, g') = random g

runSF inputs reactimate toInput withOutput sf = do
  countRef <- newIORef stepCount
  inputRef <- newIORef inputs
  let input = do
        (input : inputs) <- readIORef inputRef
        writeIORef inputRef inputs
        pure (1, toInput input)
      output !val = do
        count <- readIORef countRef
        writeIORef countRef (count - 1)
        () <- withOutput val
        pure (count <= 0)
  reactimate input output sf

force val =
  case rnf val of
    () -> pure ()

main :: IO ()
main = do
  stdGen <- newStdGen
  (arg : remaining) <- getArgs
  let inputs = mkInputs stdGen
      !_ = drop stepCount inputs
      actSFRP = runSF inputs ClassSF.reactimate ClassSF.CC
                (force :: ClassSF.SDVal ('ClassSF.C Double) -> IO ()) (go (read arg :: Double))
      actYampa = runSF inputs runYampa Just
                 (force :: Double -> IO ()) (goYampa (read arg :: Double))
  withArgs remaining $
    defaultMain [ bgroup "sfrp"
                  [ bench ""
                    (nfIO actSFRP)
                  ]
                , bgroup "yampa"
                  [ bench ""
                    (nfIO actYampa)
                  ]
                ]
  -- (grouped, _) <-
  --   weighResults (do action "sfrp" actSFRP
  --                    actSFRP "yampa" actYampa)
  where runYampa input output =
          Yampa.reactimate (pure 0) (\_ -> input) (\_ -> output)

























