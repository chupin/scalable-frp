{-# LANGUAGE FlexibleContexts, OverloadedLists #-}

-- 'matchSigs' computes a transformation between any two trees A and B (if no leaf of B is
-- in A).

module ComputeRouter where

import Data.Maybe
import Data.Map (Map)
import qualified Data.Map as M
import Control.Monad.State
import Control.Monad.Writer hiding (First)
import Control.Exception
import Data.Set (Set)
import qualified Data.Set as S
import Data.Sequence (Seq(..))

import Syntax
import           Tagged

---------------------------------------------------------------------------
-- Routers
---------------------------------------------------------------------------

data Router = Id
            | Fst
            | Snd
            | UncancelFst
            | UncancelSnd
            | Drop
            | RightRot
            | LeftRot
            | Swap
            | Dup
            | AndThen Router Router
            | First Router
            | Second Router
            deriving(Show)

instance Semigroup Router where
  (<>) = AndThen

instance Monoid Router where
  mempty = Id

-- Simplifies a router
simplifyRouter :: Router -> Router
simplifyRouter (r1 `AndThen` r2) =
  case (simplifyRouter r1, simplifyRouter r2) of
    (Id, r2)                 -> r2
    (r1, Id)                 -> r1
    (UncancelFst, Fst)       -> Id
    (UncancelSnd, Snd)       -> Id
    (LeftRot, RightRot)      -> Id
    (RightRot, LeftRot)      -> Id
    (Swap, Swap)             -> Id
    (Drop, Drop)             -> Drop
    (First tr1, First tr2)   -> simplifyRouter (First (simplifyRouter (tr1 `AndThen` tr2)))
    (Second tr1, Second tr2) -> simplifyRouter (Second (simplifyRouter (tr1 `AndThen` tr2)))
    (r1, r2)                 -> r1 `AndThen` r2
simplifyRouter (First r) =
  case simplifyRouter r of
    Id -> Id
    r  -> First r
simplifyRouter (Second r) =
  case simplifyRouter r of
    Id -> Id
    r  -> Second r
simplifyRouter rout = rout

-- Compute the application of a router to a signal. Errors out if this is
-- nonsensical.
applyRouter :: Show sig
            => Router
            -> Sig (Var sig)
            -> Sig (Var sig)
applyRouter Id tree = tree
applyRouter Fst (Vec left _) = left
applyRouter UncancelFst sig = Vec sig (Sig Null)
applyRouter UncancelSnd sig = Vec (Sig Null) sig
applyRouter Snd (Vec _ right) = right
applyRouter Drop _ = Sig Null
applyRouter RightRot (Vec (Vec leftLeft leftRight) right) =
  Vec leftLeft (Vec leftRight right)
applyRouter LeftRot (Vec left (Vec rightLeft rightRight)) =
  Vec (Vec left rightLeft) rightRight
applyRouter Swap (Vec left right) = Vec right left
applyRouter Dup tree = Vec tree tree
applyRouter (r1 `AndThen` r2) tree = applyRouter r2 (applyRouter r1 tree)
applyRouter (First r) (Vec left right) = Vec (applyRouter r left) right
applyRouter (Second r) (Vec left right) = Vec left (applyRouter r right)
applyRouter r t =
  error ("Cannot apply router " ++ show r ++ " to tree:\n" ++ show t)

---------------------------------------------------------------------------
-- Statement grouping
---------------------------------------------------------------------------
--
-- Groups statements by independant blocks. Statements within an
-- independent block may be composed in parallel with ***
--
---------------------------------------------------------------------------

groupStatements :: Ord id
                => Seq (Statement (Var id) (Var id))
                -> Seq (Seq (Statement (Var id) (Var id)))
groupStatements = go [] []
  where go _ Empty Empty = []
        go _ group Empty = [group]
        go bound group (stmt@Statement { stmtInput = input
                                       , stmtOutput = output
                                       } :<| stmts)
          | S.null (inputVars `S.intersection` bound) =
              go (outputVars `S.union` bound) (stmt :<| group) stmts
          | otherwise = group :<| go outputVars [stmt] stmts
          where inputVars = M.keysSet (sigVars input)
                outputVars = M.keysSet (sigVars output)

---------------------------------------------------------------------------
-- Stack of used variables
---------------------------------------------------------------------------

-- The set of variables used in a list of statements
stackUsedVars :: Ord id
              => Seq (Statement (Var id) (Var id))
              -> Seq (Set id)
stackUsedVars = fmap (M.keysSet . sigVars . stmtInput)

---------------------------------------------------------------------------
-- Computing the glue
---------------------------------------------------------------------------

---------------------------------------------------------------------------
-- Reduce a tree to minimal form. In the resulting tree there are:
--
--  * Only variables that are « needed ». The dictionnary contains
--    these needed variables as well as the number that should appear
--    of each of them. All occurences of the same variable are grouped
--    in a single subtree.
--
--  * No Null signal unless there are no needed variables, in which
--    case the whole reduced signal is Sig Null.
--
---------------------------------------------------------------------------

-- Compute the transformation needed to duplicate the signal as many time as
-- there are indices. The indices are used to index the signals.
duplicateAndIndex :: [Index] -> sig -> (Sig (Indexed sig), Router)
duplicateAndIndex [idx] sig = (Sig (sig `tagWith` idx), Id)
duplicateAndIndex (idx : indices) sig = ( Vec (Sig (sig `tagWith` idx)) subSig
                                        , Dup `AndThen` Second subTr
                                        )
  where (subSig, subTr) = duplicateAndIndex indices sig

insideOut :: Functor f => Indexed (f id) -> f (Indexed id)
insideOut Tagged { tag = idx
                 , taggedValue = val
                 } = (`tagWith` idx) <$> val

outsideIn :: Sig (Var (Indexed id)) -> Sig (Indexed (Var id))
outsideIn (Sig (Var (Tagged id idx) ann)) = Sig (Tagged (Var id ann) idx)
outsideIn (Vec left right) = Vec (outsideIn left) (outsideIn right)

type ReducerM id = State (Map id [Index])

-- Maps a signal to the indices it is identified with in an indexed signal
indexMap :: Ord sig => Sig (Indexed sig) -> Map sig [Index]
indexMap (Sig (Tagged sig idx)) = [(sig, [idx])]
indexMap (Vec left right) = M.unionWith (++) (indexMap left) (indexMap right)

unitaryIndexMap :: Ord sig => Sig (Indexed sig) -> Map sig [Index]
unitaryIndexMap = fmap (take 1) . indexMap

-- Remove unecessary signals from a signal, given the set of signal needed in
-- the destination tree
reduceTreeIdents :: Ord id
                 => Sig (Var id)
                 -> ReducerM id (Sig (Var (Indexed id)), Router)
reduceTreeIdents (Sig sig) = do
  needed <- get
  case sig of
    Var id _
      | Just indices <- M.lookup id needed -> do
                 modify (M.delete id)
                 let (dupSig, router) = duplicateAndIndex indices sig
                 pure (fmap insideOut dupSig, router)
    _ -> pure (Sig Null, Drop)
reduceTreeIdents (Vec left right) = do
  (left, trLeft) <- reduceTreeIdents left
  (right, trRight) <- reduceTreeIdents right
  case (left, right) of
    (Sig Null, Sig Null) ->
      pure (Sig Null, Drop)
    (Sig Null, right) ->
      pure (right, Snd `AndThen` trRight)
    (left, Sig Null) ->
      pure (left, Fst `AndThen` trLeft)
    (left, right) ->
      pure (Vec left right, First trLeft `AndThen` Second trRight)

-- Same as the above, but with null signals
reduceNulls :: Show id
            => [Index]
            -> Sig (Var (Indexed id))
            -> (Sig (Indexed (Var id)), Router)
reduceNulls [] sig = (outsideIn sig, Id)
reduceNulls indices sig =
  case sig of
    Sig Null -> (nulls, routerNulls)
    _ -> (Vec (outsideIn sig) nulls, UncancelFst `AndThen` Second routerNulls)
  where (nulls, routerNulls) = duplicateAndIndex indices Null

-- Reduce a tree so that it has the right number of signals. Each signal
-- variable has a unique index, which is the order it should appear in in the
-- final tree
reduceTree :: (Show id, Ord id)
           => Map (Var id) [Index]
           -> Sig (Var id)
           -> (Sig (Indexed (Var id)), Router)
reduceTree sigIndices sig =
  (fullyReduced, identReducedRouter `AndThen` nullReducedRouter)
  where identIndices=
          M.foldrWithKey (\sig indices map ->
                            case sig of
                              Var id _ -> M.insert id indices map
                              _ -> map) [] sigIndices
        (identReduced, identReducedRouter) =
          evalState (reduceTreeIdents sig) identIndices
        (fullyReduced, nullReducedRouter) =
          reduceNulls (fromMaybe [] (M.lookup Null sigIndices)) identReduced

---------------------------------------------------------------------------
-- Transforms an arbitrarly shaped tree in a list shaped tree, where
-- all leaves but the last are on left on a node.
--
-- Ex:
--     /\
--    /  \
--   /    \
--  /\    /\
-- 0  1  2  3
--
-- Becomes:
--
--  /\
-- 0 /\
--  1 /\
--   2  3
---------------------------------------------------------------------------

listify :: Sig sig -> ([sig], Router)
listify (Sig sig) = ([sig], Id)
listify (Vec (Sig sig) right) = (sig : listRight, Second trans)
  where (listRight, trans) = listify right
listify (Vec (Vec leftLeft leftRight) right) = (list, RightRot `AndThen` trans)
  where rotatedSig = Vec leftLeft (Vec leftRight right)
        (list, trans) = listify rotatedSig

delistify :: [sig] -> Sig sig
delistify [] = error "Impossible"
delistify [sig] = Sig sig
delistify (sig : xs) = Vec (Sig sig) (delistify xs)

unsnoc :: [a] -> Maybe ([a], a)
unsnoc [] = Nothing
unsnoc (x : xs) =
  case unsnoc xs of
    Just (ys, y) -> Just (x : ys, y)
    Nothing -> Just ([], x)

bubbleSort :: Ord a => [a] -> ([a], Router)
bubbleSort list = runWriter $ go True [] list
  where go firstPass sortedSndSubList unsortedFstSubList =
          case unsnoc sortedFstSubList of
            Nothing -> pure sortedSndSubList
            Just (initSortedFstSubList, unmovableLastElement) -> do
              tell routerSortFstSubList
              go False (unmovableLastElement : sortedSndSubList) initSortedFstSubList
          where (sortedFstSubList, routerSortFstSubList) =
                  sortPass firstPass unsortedFstSubList

        -- Does one pass along the tree. Uses the firstPass boolean to
        -- know what to do with the last element in case it's not in
        -- the right order, since Swap only makes sense if we are
        -- obversving the two last elements of the tree (which only
        -- happens in the first pass)
        sortPass _ [] = ([], Id)
        sortPass _ [x]  = ([x], Id)
        sortPass firstPass (x : xs@(y : ys)) = (z : sortedZs, tr `AndThen` Second zstr)
          where (z, zs, tr)
                  | x <= y = (x, xs, Id)
                  | otherwise = (y, (x : ys), case ys of
                                                [] | firstPass -> Swap
                                                _ -> swapHeads)
                (sortedZs, zstr) = sortPass firstPass zs

        swapHeads = LeftRot `AndThen` First Swap `AndThen` RightRot

---------------------------------------------------------------------------
-- Tagging and untagging signals
---------------------------------------------------------------------------

tagSig :: Sig sig -> Sig (Indexed sig)
tagSig sig = evalState (go sig) 0
  where go (Sig sig) = do
          idx <- get
          put (idx + 1)
          pure (Sig (Tagged sig idx))
        go (Vec l r) = do
          l <- go l
          r <- go r
          pure (Vec l r)

detagSig :: Sig (Tagged tag a) -> Sig a
detagSig = fmap taggedValue

---------------------------------------------------------------------------
-- Compute the router needed to give the first tree the same shape as
-- the second tree provided they have the same number of leaves. Quite
-- a dump algorithm
---------------------------------------------------------------------------

matchShapes :: Show id => Sig (Var id) -> Sig (Var id) -> Router
matchShapes Sig{} Sig{} = Id
matchShapes start@(Vec l1 r1) dest@(Vec l2 r2) =
  case assert (cl1 + cr1 == cl2 + cr2) (cl1 `compare` cl2) of
    EQ -> First (matchShapes l1 l2) `AndThen` Second (matchShapes r1 r2)
    -- There are more nodes on the left that there should. We should
    -- thus do a right rotation and try to match again
    GT -> RightRot `AndThen` matchShapes (applyRouter RightRot start) dest
    -- The above, but in reverse
    LT -> LeftRot `AndThen` matchShapes (applyRouter LeftRot start) dest
  where cl1 = countLeaves l1
        cl2 = countLeaves l2

        cr1 = countLeaves r1
        cr2 = countLeaves r2

---------------------------------------------------------------------------
-- Computes the router for the start tree to the destinationt tree.
--
-- The algorithm goes as follow:
--
--   * Reduce the starting tree
--
--   * Tag the start tree and the destination tree with indices. Then
--   make them into indexed ordered value using the destination tree
--   order.
--
--   * Listify the start tree and bubbleSort the result
--
--   * Match the shape of the start tree to the destination tree

matchSigs :: ( Ord id
             , Show id
             )
          => Sig (Var id)
          -> Sig (Var id)
          -> Router
matchSigs start dest =
  assert (applyRouter simplifiedRouter start == dest) simplifiedRouter
  where simplifiedRouter = simplifyRouter router

        ((), router) = runWriter (go start dest)
        taggedDest = tagSig dest
        destSigIndexMap = indexMap taggedDest
        go start dest = do
          let (reducedStartTree, reducingRouter) =
                reduceTree destSigIndexMap start
          tell reducingRouter
          let ( listifiedStartTree
                , listifyingRouter
                ) = listify reducedStartTree
          tell listifyingRouter
          let (listifiedSortedStartTree, sortingRouter) =
                bubbleSort listifiedStartTree
          tell sortingRouter
          let sortedStartTree =
                detagSig $ delistify listifiedSortedStartTree
              matchingShapesRouter = matchShapes sortedStartTree dest
          tell matchingShapesRouter
          pure ()
