{

module TypParser (parseTyp) where

import Syntax
import TypLexer
import qualified Language.Haskell.Meta      as H

}

%name parseTyp Typ

%monad { Either String }
%tokentype { Tok }

%token
        typ      { TokHsTyp $$ }
        '::'     { TokDoubleCol }
        ann      { TokAnn $$ }
        '{'      { TokLBrace }
        '}'      { TokRBrace }
        ','      { TokComma }

%%

Typ: Sig { $1 }

HsTyp: typ {% H.parseType $1 }

Sig: '{' '}' { Sig Nothing }
   | HsTyp '::' ann { Sig (Just ($1, $>)) }
   | '{' Sig '}' { $2 }
   | '{' Sig ',' Sig '}' { Vec $2 $4 }

{

happyError toks = Left ("Parse error on " ++ show toks)

}
