module ToYampa where

import qualified FRP.Yampa as Y

type family YampaTyp sd where
  YampaTyp (C a) = a
  YampaTyp (S a) = a
  YampaTyp (E a) = Y.Event a
  YampaTyp (i `P` o) = (YampaTyp i, YampaTyp o)

toYampa :: SF i o -> Y.SF (YampaTyp i) (YampaTyp o)
toYampa (MkFirst sf) = first (toYampa sf)
toYampa (Router router) = undefined
toYampa (sf1 :>>>: sf2) = toYampa sf1 Y.>>> toYampa sf2
toYampa (sf1 :&&&: sf2) = toYampa sf1 Y.&&& toYampa sf2
toYampa (sf1 :***: sf2) = toYampa sf1 Y.*** toYampa sf2
toYampa (Jump init detect) =
