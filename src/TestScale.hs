{-# LANGUAGE Arrows, DataKinds, GADTs, NumericUnderscores, TemplateHaskell #-}

module Main where

import           ClassSF                    hiding (arr, integral, (<<<))
import           Control.DeepSeq
import           Control.Monad
import           Data.IORef
import           FRP.Yampa
    ( arr
    , iPre
    , integral
    , returnA
    , (<<<)
    )
import qualified FRP.Yampa                  as Y
import qualified Language.Haskell.Meta      as H
import qualified Language.Haskell.TH.Quote  as QQ
import qualified Language.Haskell.TH.Syntax as TH
import           Quote
import           Scale
import           System.Random
import           TestNet
import           Utils

networkSFRP :: SF (C Double) (C Double)
networkSFRP = $(QQ.quoteExp sigFun strNetSFRP)

networkYampa :: Y.SF Double Double
networkYampa =proc _0-> do {
(_1) <- iPre (0.5486439657327848) -< (_0);
(_2) <- iPre (0.36616173715911926) -< (_1);
(_3) <- arr (uncurry (+)) -< (_0,_2);
(_4) <- arr (uncurry (+)) -< (_2,_2);
(_5) <- iPre (1.9413460597147547e-2) -< (_0);
(_6) <- arr (uncurry (*)) -< (_3,_3);
(_7) <- arr (uncurry (*)) -< (_4,_4);
(_8) <- iPre (0.6737276331182406) -< (_1);
(_9) <- iPre (0.8074570816127277) -< (_7);
(_10) <- arr (uncurry (+)) -< (_8,_4);
(_11) <- iPre (0.11840223991752852) -< (_1);
(_12) <- arr (uncurry (+)) -< (_11,_11);
(_13) <- iPre (0.3867147376039539) -< (_4);
(_14) <- iPre (0.2210493245340679) -< (_13);
(_15) <- iPre (0.9602628933937909) -< (_4);
(_16) <- iPre (0.2169370641576771) -< (_0);
(_17) <- iPre (0.776323435054771) -< (_9);
(_18) <- iPre (0.11126279649161508) -< (_15);
(_19) <- arr (uncurry (-)) -< (_15,_12);
(_20) <- integral <<< arr (\(x, y) -> x * y) -< (_12,_12);
(_21) <- arr (uncurry (*)) -< (_7,_17);
(_22) <- arr (uncurry (-)) -< (_3,_20);
(_23) <- iPre (0.4035463617708007) -< (_15);
(_24) <- iPre (0.3998190976844763) -< (_9);
(_25) <- integral <<< arr (\(x, y) -> x * y) -< (_23,_3);
(_26) <- iPre (0.4486794778309656) -< (_24);
(_27) <- iPre (0.9962614645318632) -< (_11);
(_28) <- integral <<< arr (\(x, y) -> x * y) -< (_15,_12);
(_29) <- iPre (0.6509600269196871) -< (_19);
(_30) <- integral <<< arr (\(x, y) -> x * y) -< (_26,_8);
(_31) <- iPre (0.7089059662121368) -< (_0);
(_32) <- arr (uncurry (-)) -< (_19,_17);
(_33) <- iPre (7.894932153754075e-2) -< (_7);
(_34) <- arr (uncurry (-)) -< (_19,_24);
(_35) <- iPre (0.10670313873362292) -< (_0);
(_36) <- iPre (0.3105792044969464) -< (_30);
(_37) <- iPre (0.45378477507247883) -< (_24);
(_38) <- arr (uncurry (-)) -< (_37,_13);
(_39) <- integral <<< arr (\(x, y) -> x * y) -< (_7,_18);
(_40) <- iPre (2.085012675583886e-2) -< (_35);
(_41) <- iPre (9.421045163592046e-2) -< (_12);
(_42) <- arr (uncurry (*)) -< (_9,_23);
(_43) <- arr (uncurry (-)) -< (_18,_29);
(_44) <- arr (uncurry (*)) -< (_4,_42);
(_45) <- integral <<< arr (\(x, y) -> x * y) -< (_5,_0);
(_46) <- iPre (0.13988503261803953) -< (_11);
(_47) <- arr (uncurry (*)) -< (_17,_4);
(_48) <- arr (uncurry (*)) -< (_2,_47);
(_49) <- arr (uncurry (+)) -< (_13,_23);
(_50) <- iPre (0.8209049192122242) -< (_28);
(_51) <- iPre (0.3253817876047711) -< (_29);
(_52) <- arr (uncurry (-)) -< (_21,_45);
(_53) <- arr (uncurry (-)) -< (_36,_16);
(_54) <- arr (uncurry (-)) -< (_41,_22);
(_55) <- arr (uncurry (-)) -< (_25,_38);
(_56) <- arr (uncurry (+)) -< (_43,_49);
(_57) <- integral <<< arr (\(x, y) -> x * y) -< (_54,_44);
(_58) <- arr (uncurry (*)) -< (_33,_52);
(_59) <- integral <<< arr (\(x, y) -> x * y) -< (_57,_46);
(_60) <- arr (uncurry (+)) -< (_32,_39);
(_61) <- arr (uncurry (-)) -< (_53,_27);
(_62) <- arr (uncurry (*)) -< (_50,_61);
(_63) <- arr (uncurry (+)) -< (_58,_31);
(_64) <- arr (uncurry (*)) -< (_51,_62);
(_65) <- integral <<< arr (\(x, y) -> x * y) -< (_60,_56);
(_66) <- integral <<< arr (\(x, y) -> x * y) -< (_6,_55);
(_67) <- integral <<< arr (\(x, y) -> x * y) -< (_64,_48);
(_68) <- arr (uncurry (-)) -< (_67,_40);
(_69) <- arr (uncurry (-)) -< (_63,_14);
(_70) <- arr (uncurry (*)) -< (_66,_65);
(_71) <- arr (uncurry (*)) -< (_69,_68);
(_72) <- arr (uncurry (-)) -< (_34,_71);
(_73) <- arr (uncurry (+)) -< (_59,_70);
(_74) <- integral <<< arr (\(x, y) -> x * y) -< (_10,_72);
(_75) <- arr (uncurry (-)) -< (_74,_73);

returnA -< _75}

runSFRP :: Int -> SF (C Double) (C Double) -> IO ()
runSFRP n sf = go n =<< initializeEngine (compile sf)
  where go :: Int -> ExecutionEngine (C Double) (C Double) -> IO ()
        go 0 _ = pure ()
        go n engine = do
          (CC o, engine) <- stepEngine engine (CC 0)
          case rnf o of
            () -> go (n - 1) engine

runYampa :: Int -> Y.SF Double Double -> IO ()
runYampa n sf = do
  countRef <- newIORef n
  Y.reactimate
    (pure 0)
    (\_ -> pure (0.1, Just 0))
    (\_ b -> case rnf b of
               () -> do
                 count <- readIORef countRef
                 if count == 0
                   then pure True
                   else do
                   writeIORef countRef (count - 1)
                   pure False)
    sf


count :: Int
count = 1_000_000

main :: IO ()
main = runSFRP count networkSFRP
