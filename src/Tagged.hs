module Tagged where


-- A value with a tag. The tag is used for the 'Eq' and 'Ord' instances
data Tagged tag a = Tagged { taggedValue :: a
                           , tag         :: tag
                           }

tagWith :: a -> tag -> Tagged tag a
value `tagWith` tag = Tagged { taggedValue = value
                             , tag = tag
                             }

instance Eq tag => Eq (Tagged tag a) where
  Tagged { tag = i1 } == Tagged { tag = i2 } =
    i1 == i2

instance Ord tag => Ord (Tagged tag a) where
  Tagged { tag = i1 } `compare` Tagged { tag = i2 } =
    i1 `compare` i2

instance (Show tag, Show a) => Show (Tagged tag a) where
  show (Tagged v idx) = "(" ++ show v ++ ":" ++ show idx ++ ")"

type Index = Int

type Indexed a = Tagged Index a
