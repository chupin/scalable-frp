{-# LANGUAGE ApplicativeDo, DataKinds, FlexibleContexts, GADTs, KindSignatures,
             LambdaCase, NoStarIsType, OverloadedLists, PolyKinds,
             RecordWildCards, TemplateHaskell, TypeOperators #-}

module Main where

import qualified ClassSF                    as SFRP
import           Control.Lens               hiding (argument)
import           Control.Monad.Reader
import           Control.Monad.State
import           Data.Foldable
import           Data.List                  (intercalate)
import           Data.Proxy
import           Data.Sequence              (Seq (..))
import           Data.Set                   (Set)
import qualified Data.Set                   as S
import           GHC.TypeLits
import qualified Language.Haskell.TH.Ppr    as H
import qualified Language.Haskell.TH.Quote  as H
import qualified Language.Haskell.TH.Syntax as H
import           Options.Applicative
import qualified Quote                      as SFRP
import           Syntax                     hiding (S, Var)
import qualified Syntax                     as SFRP
import           System.Environment
import           Test.QuickCheck            hiding (Fun)

import           Debug.Trace

newtype Var = Var Int
            deriving(Eq, Ord)

instance Show Var where
  show (Var n) = '_' : show n

data Op = Plus | Minus
        deriving(Enum, Bounded)

instance Show Op where
  show Plus  = "+"
  show Minus = "-"

instance Arbitrary Op where
  arbitrary = oneof (fmap pure [Plus, Minus])

data S = S { _targetSize :: Int
           , _usedVars   :: Set Var
           , _scopedVars :: Set Var
           }

makeLenses ''S

data R = R { _switchFreq    :: Int
           , _switchingFreq :: Int
           }

makeLenses ''R

type M = StateT S (ReaderT R Gen)

liftGen :: Gen a -> M a
liftGen = lift . lift

data SwitchConf = SwitchConf { switchSlide :: Int }

data Block = Join Op | Integr | Switch SwitchConf Network

arity :: Block -> Int
arity Join {}   = 2
arity Integr {} = 1
arity Switch {} = 1

arbitraryBlock :: M Block
arbitraryBlock = do
  size <- use targetSize
  scope <- use scopedVars
  if size <= 1 || length scope <= 1
    then do
    block <- liftGen $ oneof [ genJoin
                             , pure Integr
                             ]
    targetSize %= pred
    pure block
    else do
    switchSize <- liftGen $ choose (1, size `div` 2)
    () <- traceShow switchSize (pure ())
    env <- ask
    let swFreq = env ^. switchFreq
    block <-
      liftGen $ if length scope <= 1
                then frequency [ (swFreq, genSwitch env switchSize)
                               , (100 - swFreq, pure Integr)
                               ]
                else frequency [ (swFreq, genSwitch env switchSize)
                               , ((100 - swFreq) `div` 2, pure Integr)
                               , ((100 - swFreq) `div` 2, genJoin)
                               ]
    targetSize %= subtract (blockSize block)
    pure block
  where genSwitch env size = do
          let windowSize = min (env ^. switchingFreq `div` 2) 10
          slide <- choose (- windowSize, windowSize)
          Switch SwitchConf { switchSlide = env ^. switchingFreq + slide } <$>
            arbitraryNet env size
        genJoin = Join <$> arbitrary

data Line = Line { lineBlock :: Block
                 , lineArgs  :: [Var]
                 , lineRet   :: [Var]
                 }

arbitraryLine :: Bool -> M Line
arbitraryLine forceJoin = do
  block <- if forceJoin
           then liftGen (Join <$> arbitrary)
           else arbitraryBlock
  let inputCt = arity block
  outputVar <- liftGen (Var <$> choose (1, maxBound))
  scope <- use scopedVars
  inputVars <- liftGen $ replicateM inputCt (oneof (fmap pure (toList scope)))
  usedVars %= S.union (S.fromList inputVars)
  scopedVars %= S.insert outputVar
  pure Line { lineBlock = block
            , lineArgs = inputVars
            , lineRet = [outputVar]
            }

blockSize :: Block -> Int
blockSize (Switch _ n1) = netSize n1
blockSize _             = 1

netSize :: Network -> Int
netSize Network { netLines = lines } = sum (fmap (blockSize . lineBlock) lines)

arbitraryNet :: R -> Int -> Gen Network
arbitraryNet env size = do
  inputVar <- Var <$> choose (1, maxBound)
  (lines, outputVar) <-
    runReaderT (evalStateT manyLines S { _targetSize = size
                                       , _scopedVars = [inputVar]
                                       , _usedVars = []
                                       }) env
  let network = Network { netInput = inputVar
                        , netOutput = outputVar
                        , netLines = lines
                        }
  () <- traceShow (size, netSize network) (pure ())

  pure network
  where closeNet = do
          used <- use usedVars
          scope <- use scopedVars
          let unused = scope S.\\ used
          if length unused == 1
            then pure ([], S.findMin unused)
            else do
            -- targetSize .= 1
            line <- arbitraryLine True
            scopedVars %= (S.\\ S.fromList (lineArgs line))
            (lines, output) <- closeNet
            pure (line :<| lines, output)

        manyLines = do
          size <- use targetSize
          used <- use usedVars
          scope <- use scopedVars
          let unusedSize = length (scope S.\\ used)
          if unusedSize - 1 >= size
            then do
            -- We must close the network. We remove the used variables
            -- from the set of scoped variables and start closing
            scopedVars %= (S.\\ used)
            closeNet
            else do
            line <- arbitraryLine False
            (lines, output) <- manyLines
            pure (line :<| lines, output)

data Network = Network { netInput  :: Var
                       , netOutput :: Var
                       , netLines  :: Seq Line
                       }

printVarBlock :: [Var] -> String
printVarBlock vs  = intercalate "," (fmap show vs)

opSF :: Op -> H.Q H.Exp
opSF Plus  = [|(+)|]
opSF Minus = [|(-)|]

genBlockSFRP :: Block -> StateT [H.Dec] H.Q H.Exp
genBlockSFRP Integr    = lift [|SFRP.integr|]
genBlockSFRP (Join op) = lift [|SFRP.arr2 $(opSF op)|]
genBlockSFRP (Switch swconf swAux) = do
  swName <- lift $ H.newName "switch_go"
  swAuxName <- lift $ H.newName "go"
  let fundec name fun = do
        exp <- lift $ SFRP.quoteFun fun
        pure (H.FunD name [H.Clause [] (H.NormalB exp) []])

  decAux <- fundec swAuxName =<< genNetworkSFRP swAux
  let swSlide = switchSlide swconf
  swExp <- lift [|SFRP.switch ($(pure (H.VarE swAuxName)) SFRP.>>>
                               ((SFRP.constA 1 SFRP.>>>
                                 SFRP.imIntegr (- swSlide) SFRP.>>>
                                 SFRP.arrC (>= 0) SFRP.>>>
                                 SFRP.edgeGuard)
                                SFRP.&&&
                                SFRP.identity))
                  (\_ -> $(pure (H.VarE swName)))|]
  let decSw = H.FunD swName [H.Clause [] (H.NormalB swExp) []]
  modify ([decAux, decSw] ++)
  pure (H.VarE swName)


toVar :: Var -> SFRP.Var String
toVar (Var n) = SFRP.Var ("_" ++ show n) Nothing

toSigUndes :: [Var] -> SFRP.Sig SFRP.UndesVar
toSigUndes [] = SFRP.Sig (SFRP.SigVar SFRP.Null)
toSigUndes [var] = SFRP.Sig (SFRP.SigVar (toVar var))
toSigUndes (v1 : vs@(_ : _)) =
  SFRP.Vec (SFRP.Sig (SFRP.SigVar (toVar v1))) (toSigUndes vs)

toSig :: [Var] -> SFRP.Sig (SFRP.Var String)
toSig []                = SFRP.Sig SFRP.Null
toSig [var]             = SFRP.Sig (toVar var)
toSig (v1 : vs@(_ : _)) = SFRP.Vec (SFRP.Sig (toVar v1)) (toSig vs)

genLineSFRP :: Line
            -> StateT [H.Dec] H.Q (SFRP.Statement UndesVar (SFRP.Var String))
genLineSFRP Line {..} = do
  exp <- genBlockSFRP lineBlock
  pure Statement { stmtInput = toSigUndes lineArgs
                 , stmtOutput = toSig lineRet
                 , stmtBody = exp
                 }

genNetworkSFRP :: Network -> StateT [H.Dec] H.Q (Fun UndesVar (SFRP.Var String))
genNetworkSFRP Network {..} = do
  statements <- traverse genLineSFRP netLines
  pure Fun { funInput = toSig [netInput]
           , funOutput = toSigUndes [netOutput]
           , funBody = statements
           }

genModuleSFRP :: String -> Network -> H.Q [H.Dec]
genModuleSFRP netName network = do
  (network, prevDecs) <- runStateT (genNetworkSFRP network) []
  exp <- SFRP.quoteFun network
  pure (H.FunD (H.mkName netName) [H.Clause [] (H.NormalB exp) []] : prevDecs)

-- printBlockSFRP (Switch n1 n2) =
--   "switch " ++ "[sigFun|" ++ printSFRP n1 ++ "|]" ++
--   "(const ([sigFun|" ++ printSFRP n2 ++ "|]))"

printBlockYampa :: Block -> String
printBlockYampa Integr         =
  "(Yampa.integral :: Yampa.SF Double Double)"
printBlockYampa (Join op)      =
  "(Yampa.arr (uncurry (" ++ show op ++ ")) :: Yampa.SF (Double,Double) Double)"
printBlockYampa (Switch swconf swAux) =
  "(let swfun = Yampa.switch ((" ++ printYampaNetwork swAux ++
  ") Yampa.>>> (Yampa.identity Yampa.&&& \
  \(Yampa.constant (1 :: Double) Yampa.>>> Yampa.integral Yampa.>>> \
  \Yampa.arr (subtract " ++
  show (switchSlide swconf) ++ ") Yampa.>>> up)))" ++
  "(\\_ -> swfun) in swfun)"

printLine :: (Block -> String) -> String -> String -> String -> Line -> String
printLine pblock sepLeft sepRight lineEnd Line { .. } =
  sepLeft ++ printVarBlock lineRet ++ sepRight ++ " <- " ++
  pblock lineBlock ++ " -< " ++
  sepLeft ++ printVarBlock lineArgs ++ sepRight ++ lineEnd

printLineYampa :: Line -> String
printLineYampa = printLine printBlockYampa "(" ")" ";"

-- printSFRP :: Network -> String
-- printSFRP Network { .. } =
--   "proc " ++ show netInput ++ " do" ++ "\n" ++
--   unlines (toList (fmap printLineSFRP netLines)) ++ "\n" ++
--   "out " ++ show netOutput

printYampaNetwork :: Network -> String
printYampaNetwork Network {..} =
  "proc " ++ show netInput ++ "-> do {\n" ++
  unlines (toList (fmap printLineYampa netLines)) ++ "\n" ++
  "Yampa.returnA -< " ++ show netOutput ++ "}"

printYampa :: String -> Network -> String
printYampa name network =
  name ++ " = " ++ printYampaNetwork network

data Config = Config { genSize              :: Int
                     , genSwitchFreq        :: Int
                     , genSwitchingFreq     :: Int
                     , genTopLevelSFRPName  :: String
                     , genTopLevelYampaName :: String
                     , genOutputSFRPFile    :: FilePath
                     , genOutputYampaFile   :: FilePath
                     }

configParser :: Parser Config
configParser = do
  genSize <- argument auto (metavar "SIZE" <>
                            help "The size of the network to generate")
  genSwitchFreq <-
    argument auto (metavar "SWITCH_FREQ" <>
                    help "The frequency (as a percentage) at which \
                         \a switch should be generated.")
  genSwitchingFreq <-
    argument auto (metavar "SWITCHING_FREQ" <>
                    help "The number of iterations between two switching events")
  genTopLevelSFRPName <- argument str (metavar "TL_NAME")
  genTopLevelYampaName <- argument str (metavar "YAMPA_NAME")
  genOutputSFRPFile <- argument str (metavar "OUTPUT_SFRP")
  genOutputYampaFile <- argument str (metavar "OUTPUT_YAMPA")
  pure Config {..}

main :: IO ()
main = do
  let configInfo =
        info (helper <*> configParser)
        (fullDesc <> progDesc "A generator for abitrary FRP network")
  Config { genSwitchFreq = switchFreq
         , genSwitchingFreq = switchingFreq
         , genSize = size
         , genTopLevelYampaName = yampaName
         , genTopLevelSFRPName = sfrpName
         , genOutputSFRPFile = outSFRP
         , genOutputYampaFile = outYampa
         } <- execParser configInfo
  let env = R { _switchingFreq = switchingFreq
              , _switchFreq = switchFreq
              }
  network <- generate (arbitraryNet env size)
  writeFile outYampa (printYampa yampaName network)
  decs <- H.runQ (genModuleSFRP sfrpName network)
  writeFile outSFRP (H.pprint decs)

-- main :: IO ()
-- main = do
--   (size : yampa : _) <- getArgs
--   gen <- getStdGen
--   let net = randomNetwork gen (read size)
--       str = case yampa of
--               'y' : _ -> printYampa net
--               _       -> printSFRP net
--   putStrLn str
