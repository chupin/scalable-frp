{-# LANGUAGE DataKinds, PartialTypeSignatures, QuasiQuotes, TypeOperators #-}

module Utils where

import ClassSF
import Quote

---------------------------------------------------------------------------
-- Pre and initialization
---------------------------------------------------------------------------

initialize :: (IsSD a, ToSD a) => Raw a -> SF a a
initialize init = dswitch aux (const identity)
  where aux = [sigFun|
                     proc input do
                        evt <- constA (Just ()) -< input;
                        initSig <- constA init -< input;
                        out {evt, initSig}
                        |]

pre :: (ToSD a, IsSD a) => Raw a -> SF a a
pre xn1 = loop go >>> initialize xn1
  where go = [sigFun|
               proc {xn, xn1} do
               out {xn1, xn}
                 |]
