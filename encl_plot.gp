set autoscale
unset log
unset label
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set y2tic auto
#set title "Average running time and speedup (100 000 iterations)"
set xlabel "Network size"
set ylabel "Mean time (s)"
set y2label "SFRP over Yampa speedup"
set xrange [0:320]
set y2range [0:30]
set yrange [0:25]
#set yrange [0:]

if (ARG1 eq 'tikz') {
    set terminal tikz size 8.5,6 createstyle
} else {
    set terminal postscript color
}

# sfrp/ yampa/
types = "sfrp/ yampa/"
# 0 5 10 50 100 1000 10000
switching_freqs="50"# 50 100000" #"100"# 100"
type_names = "\"SFRP\" Yampa"
file="bench/all_bench.csv"

set key center top # bottom outside
#set datafile separator ","

condition(type_idx, switching_freq_idx)=\
sprintf("($3 == \"%s\" && $2 == %d)",\
    word(types, type_idx),\
    word(switching_freqs, switching_freq_idx) + 0)

awk_cmd(type_idx, switching_freq_idx)=\
sprintf("<awk -F, '{if %s {print $1,$4,$2}}' bench/encl_all_bench.csv",\
        condition(type_idx, switching_freq_idx))

# set dgrid3d 30,30
# set hidden3d

set style line 5 lt rgb "cyan"
set style line 6 lt rgb "magenta"

# start value for H
h1 = 117/360.0
# end value for H
h2 = 250/360.0
# creating the palette by specifying H,S,V
set palette model HSV functions (1-gray)*(h2-h1)+h1,1,0.68
condition_rat(switching_freq_idx)=\
sprintf("($3 == %d)",\
    word(switching_freqs, switching_freq_idx) + 0)

awk_cmd_rat(switching_freq_idx)=\
sprintf("<awk -F, '{if %s {print $2,$4}}' bench/encl_ratio_bench.csv",\
        condition_rat(switching_freq_idx))

condition(type_idx, switching_freq_idx)=\
sprintf("($3 == \"%s\" && $2 == %d)",\
    word(types, type_idx),\
    word(switching_freqs, switching_freq_idx) + 0)

awk_cmd(type_idx, switching_freq_idx)=\
sprintf("<awk -F, '{if %s {print $1,$4}}' bench/encl_all_bench.csv",\
        condition(type_idx, switching_freq_idx))

plot for[switching_freq=1:words(switching_freqs)] \
     awk_cmd_rat(switching_freq) \
     using 1:2 \
     with  linespoints \
     axes x1y2 \
     title sprintf("Speedup"),\
     for[type=1:words(types)] \
     for[switching_freq=1:words(switching_freqs)] \
     awk_cmd(type, switching_freq) \
     using 1:2 \
     with  linespoints \
     axes x1y1 \
     title sprintf("%s", word(type_names, type))
     # , every %s, freq %s",\
     #               word(type_names, type),\
     #               word(switching_freqs, switching_freq),\
     #               word(switch_freqs, switch_freq))

     # , every %s, freq %s",\
     #               word(type_names, type),\
     #               word(switching_freqs, switching_freq),\
     #               word(switch_freqs, switch_freq))

# plot for[type=1:words(types)] \
#      for[switching_freq=1:words(switching_freqs)] \
#      awk_cmd(type, switching_freq) \
#      using 1:2:3\
#      with  linespoints lc palette \
#      title sprintf("%s, every %s", word(type_names, type), word(switching_freqs, switching_freq))
