set autoscale
unset log
unset label
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set title "Speedup (%) (100 000 iterations)"
set xlabel "Switch every"
set ylabel "Speedup (%)"
#set xrange [0:100]
set yrange [0:] #18]

if (ARG1 eq 'tikz') {
    set terminal tikz size 8.5,6 createstyle
} else {
    set terminal postscript color
}

# # sfrp/ yampa/
# types = "sfrp/ yampa/"
# # 0 10 25 50 75 95
# switch_freqs="95"
# # 0 5 10 50 100 1000 10000
# switching_freqs="5 10 50 100 1000 10000 100000"
# type_names = "\"SFRP\" Yampa"
sizes="5 10 25 50 100 150 200 250 300"
set key left # bottom outside
#set datafile separator ","

condition(size_idx)=\
sprintf("($2 == %d && $3 != '100000')", word(sizes, size_idx) + 0)

awk_cmd(size_idx)=\
sprintf("<awk -F, '{if %s {print (100000/$3),$4}}' bench/encl_ratio_bench.csv",\
        condition(size_idx))

plot for[size_idx=1:words(sizes)] \
     awk_cmd(size_idx) \
     using 1:2 \
     with  linespoints \
     title sprintf("Size %d", 0 + word(sizes, size_idx))
     # , every %s, freq %s",\
     #               word(type_names, type),\
     #               word(switching_freqs, switching_freq),\
     #               word(switch_freqs, switch_freq))
