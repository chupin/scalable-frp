#!/bin/bash

FUN_NAME=go
YAMPA_FUN_NAME=goYampa
OUTPUT_FILE_SFRP=/tmp/Lol.hs
OUTPUT_FILE_YAMPA=/tmp/LolYampa.hs
ALL_CSVS=bench/encl_all_bench.csv

echo "Size,SwitchingEvery,Name,Mean,MeanLB,MeanUB,Stddev,StddevLB,StddevUB" > $ALL_CSVS

cabal new-configure --enable-optimization --disable-profiling

for SIZE in 5 10 25 50 100 150 200 250 300; do

    #export INVOC="stack run --allow-different-user"
    export INVOC="cabal new-run"

    eval $INVOC encl_scale -- $SIZE 0 0 \
         $FUN_NAME $YAMPA_FUN_NAME \
         $OUTPUT_FILE_SFRP $OUTPUT_FILE_YAMPA --enclose

    echo "" >> src/EnclRunTest.hs

    for SWITCHING_EVERY in 1 5 10 50 100000; do
        echo "Switch frequency: " $SWITCH_FREQ ","\
             "switch every: " $SWITCHING_EVERY " iterations,"\
             "network size: " $SIZE
        FILE=bench/bench_result_$SWITCH_FREQ\_$SWITCHING_EVERY\_$SIZE
        echo $FILE
        TXT_FILE=$FILE.txt
        CSV_FILE=$FILE.csv
        rm -f $CSV_FILE

        eval $INVOC encl_run_test -- $SWITCHING_EVERY --csv $CSV_FILE

        awk -v f="$SWITCHING_EVERY" -F, '{$1=f FS $1;}1' OFS=, $CSV_FILE |\
            awk -v f="$SIZE" -F, '{$1=f FS$1;}1' OFS=, |\
            tail -n 2 >> $ALL_CSVS
        echo "-----------------------" >> $TXT_FILE
        cat /tmp/LolYampa.hs >> $TXT_FILE
    done
done
