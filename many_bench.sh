#!/bin/bash

FUN_NAME=go
YAMPA_FUN_NAME=goYampa
OUTPUT_FILE_SFRP=/tmp/Lol.hs
OUTPUT_FILE_YAMPA=/tmp/LolYampa.hs
ALL_CSVS=bench/all_bench.csv

echo "Size,SwitchFreq,SwitchingEvery,Name,Mean,MeanLB,MeanUB,Stddev,StddevLB,StddevUB" > $ALL_CSVS

cabal new-configure --enable-optimization --disable-profiling

for SWITCH_FREQ in 0 10 25 50 75 95; do
    if [ $SWITCH_FREQ -eq 0 ]
    then ALL_SWITCHING_EVERY="100000"
    else ALL_SWITCHING_EVERY="5 10 50 100 1000 10000 100000"
    fi
    for SWITCHING_EVERY in $ALL_SWITCHING_EVERY; do
        for SIZE in 5 10 25 50 100 150 200 250 300; do
            echo "Switch frequency: " $SWITCH_FREQ ","\
                 "switch every: " $SWITCHING_EVERY " iterations,"\
                 "network size: " $SIZE
            FILE=bench/bench_result_$SWITCH_FREQ\_$SWITCHING_EVERY\_$SIZE
            echo $FILE
            TXT_FILE=$FILE.txt
            CSV_FILE=$FILE.csv
            rm -f $CSV_FILE

            #export INVOC="stack run --allow-different-user"
            export INVOC="cabal new-run"

            eval $INVOC scale -- $SIZE $SWITCH_FREQ $SWITCHING_EVERY \
                 $FUN_NAME $YAMPA_FUN_NAME \
                 $OUTPUT_FILE_SFRP $OUTPUT_FILE_YAMPA
            echo "" >> src/RunTest.hs
            eval $INVOC run_test -- --csv $CSV_FILE

            awk -v f="$SWITCHING_EVERY" -F, '{$1=f FS $1;}1' OFS=, $CSV_FILE |\
                awk -v f="$SWITCH_FREQ" -F, '{$1=f FS $1;}1' OFS=, |\
                awk -v f="$SIZE" -F, '{$1=f FS$1;}1' OFS=, |\
                tail -n 2 >> $ALL_CSVS
            echo "-----------------------" >> $TXT_FILE
            cat /tmp/LolYampa.hs >> $TXT_FILE
        done
    done
done
