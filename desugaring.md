# Initial idea

Take the input I, duplicate I and pass one of the copy to F after
shuffling (S) to match the expected input for F. You are then given `{I,
O}` as the output.

I -----> | -----------------> | I
         |                    |
         |                    | +
         |                    |
         | ---> S ---> F ---> | O

The problem we this is that we quickly get very large trees using this
technique.

The idea is then to simplify I + O to obtain the minimal tree
necessary for F *and* for all the other statements.

Let's denote by N(i) the variables needed by statement number i. We
overload this notation by writing N(i..j) to be the necessary
variables for the statement i to j included.

Let's assume there are a total of σ statements following the current
statement.

* If N(2..σ) shares no variables with I. It means that we can avoid
  the duplication of any variables from I since they are all consumed
  by F.

* In another case, we can discard all the values not in N(1..σ), then
  duplicate that. On the first branch (the one not going through F),
  we can filter all the inputs not in N(2..σ).

I ---> Filter (`notElem` N(1..σ)) ---> | ---------> Filter (`notElem` N(2..σ)) -----------> | I
                                       |                                                    |
                                       |                                                    | +
                                       |                                                    |
                                       | ---> S ---> F ---> Filter (`notElem` N(2..σ)) ---> | O
