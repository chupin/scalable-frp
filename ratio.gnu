set autoscale
unset log
unset label
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set y2tic auto
#set title "Average runtime and speedup (100 000 iterations)"
set xlabel "Network size"
set ylabel "Mean time (s)"
set y2label "SFRP over Yampa speedup"
set xrange [0:320]
set yrange [0:18]
set y2range [0:40]

if (ARG1 eq 'tikz') {
    set terminal tikz size 8.5,6 createstyle
} else {
    set terminal postscript color
}

# sfrp/ yampa/
types = "sfrp/ yampa/"
# 0 10 25 50 75 95
switch_freqs="0"
# 0 5 10 50 100 1000 10000
switching_freqs="100000" #50" #5 10 50 100 1000 10000 100000"
type_names = "\"SFRP\" Yampa"

set key center top # bottom outside
#set datafile separator ","

condition_rat(switch_freq_idx, switching_freq_idx)=\
sprintf("($3 == %d && $4 == %d)",\
    word(switch_freqs, switch_freq_idx) + 0,\
    word(switching_freqs, switching_freq_idx) + 0)

awk_cmd_rat(switch_freq_idx, switching_freq_idx)=\
sprintf("<awk -F, '{if %s {print $2,$5}}' bench/ratio_bench.csv",\
        condition_rat(switch_freq_idx, switching_freq_idx))


condition(type_idx, switch_freq_idx, switching_freq_idx)=\
sprintf("($4 == \"%s\" && $2 == %d && $3 == %d)",\
    word(types, type_idx),\
    word(switch_freqs, switch_freq_idx) + 0,\
    word(switching_freqs, switching_freq_idx) + 0)

awk_cmd(type_idx, switch_freq_idx, switching_freq_idx)=\
sprintf("<awk -F, '{if %s {print $1,$5,$8}}' bench/all_bench.csv",\
        condition(type_idx, switch_freq_idx, switching_freq_idx))

plot for[switch_freq=1:words(switch_freqs)] \
     for[switching_freq=1:words(switching_freqs)] \
     awk_cmd_rat(switch_freq, switching_freq) \
     using 1:2 \
     with  linespoints \
     axes x1y2 \
     title sprintf("Speedup"),\
     for[type=1:words(types)] \
     for[switch_freq=1:words(switch_freqs)] \
     for[switching_freq=1:words(switching_freqs)] \
     awk_cmd(type, switch_freq, switching_freq) \
     using 1:2:3 \
     with  linespoints \
     axes x1y1 \
     title sprintf("%s", word(type_names, type))
     # , every %s, freq %s",\
     #               word(type_names, type),\
     #               word(switching_freqs, switching_freq),\
     #               word(switch_freqs, switch_freq))

     # , every %s, freq %s",\
     #               word(type_names, type),\
     #               word(switching_freqs, switching_freq),\
     #               word(switch_freqs, switch_freq))
