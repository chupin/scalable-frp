set autoscale
unset log
unset label
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set title "Average running time (100 000 iterations)"
set xlabel "Network size"
set ylabel "Switch frequency"
set zlabel "Mean time (s)"
set label "Yield Point" at 0.003,260
set arrow from 0.0028,250 to 0.003,280
#set xrange [0:170]

set terminal postscript color

# sfrp/ yampa/
types = "sfrp/ yampa/"
# 0 10 25 50 75 95
switch_freqs="0 10 25 50 75 95"
# 0 5 10 50 100 1000 10000
switching_freqs="10 1000"
type_names = "\"SFRP\" Yampa"
file="bench/all_bench.csv"

set key top #bottom outside
#set datafile separator ","

condition(type_idx, switching_freq_idx)=\
sprintf("($4 == \"%s\" && $3 == %d)",\
    word(types, type_idx),\
    word(switching_freqs, switching_freq_idx) + 0)

awk_cmd(type_idx, switching_freq_idx)=\
sprintf("<awk -F, '{if %s {print $1,$5,$2}}' bench/all_bench.csv",\
        condition(type_idx, switching_freq_idx))

set dgrid3d #30,30
set hidden3d
set grid
set border 4095

splot for[type=1:words(types)] \
      for[switching_freq=1:words(switching_freqs)] \
     awk_cmd(type, switching_freq) \
     using 1:3:2 \
     with  linespoints \
     title sprintf("%s (every %s itrs)",\
                   word(type_names, type),\
                   word(switching_freqs, switching_freq))