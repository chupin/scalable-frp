import pandas as pd
import numpy as np

df = pd.read_csv('bench/all_bench.csv')

do = pd.DataFrame(columns = ['Size', 'SwitchFreq', 'SwitchingEvery', 'Ratio'])

for idx, line in df[df["Name"] == "sfrp/"].iterrows():
    size = line["Size"]
    switch_freq = line["SwitchFreq"]
    switching_every = line["SwitchingEvery"]
    mean = line["Mean"]
    y_line = df[df["Name"] == "yampa/"][df["Size"] == size][df["SwitchFreq"] == switch_freq][df["SwitchingEvery"] == switching_every]
    y_mean = y_line["Mean"]
    y_mean = y_mean.values[0]
    ratio = y_mean / mean
    do.loc[idx] = [int(size), int(switch_freq), int(switching_every), ratio]

int_col=['Size', 'SwitchFreq', 'SwitchingEvery']
do[int_col] = do[int_col].applymap(np.int64)
do.to_csv('bench/ratio_bench.csv')
