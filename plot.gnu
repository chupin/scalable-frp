set autoscale
unset log
unset label
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set title "Average running time (100 000 iterations)"
set xlabel "Network size"
set ylabel "Mean time (s)"
set xrange [0:320]
set yrange [0:18]

if (ARG1 eq 'tikz') {
    set terminal tikz size 8.5,6 createstyle
} else {
    set terminal postscript color
}

# sfrp/ yampa/
types = "sfrp/ yampa/"
# 0 10 25 50 75 95
switch_freqs="25"
# 0 5 10 50 100 1000 10000
switching_freqs="50" # 10 50 100 1000 10000 100000"
type_names = "\"SFRP\" Yampa"
file="bench/all_bench.csv"

set key left # bottom outside
#set datafile separator ","

condition(type_idx, switch_freq_idx, switching_freq_idx)=\
sprintf("($4 == \"%s\" && $2 == %d && $3 == %d)",\
    word(types, type_idx),\
    word(switch_freqs, switch_freq_idx) + 0,\
    word(switching_freqs, switching_freq_idx) + 0)

awk_cmd(type_idx, switch_freq_idx, switching_freq_idx)=\
sprintf("<gawk -F, '{if (match($0, /(%s)(.*)/, a) {print a[2],$4}}' data",\
        condition(type_idx, switch_freq_idx, switching_freq_idx))

set dgrid3d 30,30
set hidden3d

# k = (4.82 - 15.79) / (150 * 150 - 300 * 300)
# p = 15.79 - k * 300 * 300
# k2 = 15.79 / (300 * 300)
# fitp(x) = k2 * x * x
# fit(x) = k * x * x + p
a = 1.5e-4
b = 1.33e-2
c = -0.0487
fit(x) = a * x * x + b * x + c

plot for[type=1:words(types)] \
     for[switch_freq=1:words(switch_freqs)] \
     for[switching_freq=1:words(switching_freqs)] \
     awk_cmd(type, switch_freq, switching_freq) \
     using 1:2:3 \
     with  linespoints \
     axes x1y2 \
     title sprintf("%s", word(type_names, type))
     # , every %s, freq %s",\
     #               word(type_names, type),\
     #               word(switching_freqs, switching_freq),\
     #               word(switch_freqs, switch_freq))
